# -*- coding: utf-8 -*-
"""
Created on Fri Jul  6 17:56:22 2018

@author: Icaro
Script based on https://gist.github.com/bradmontgomery/2219997
"""

from http.server import BaseHTTPRequestHandler, HTTPServer
import logging
import json

recommender = None
profiler = None

def initialize():
    import pandas as pd
    import ast
    global recommender, profiler
    from recommendations import Artist_recommender, Artist_profiler
    
    data_path = '../Data/'
    df_artists = pd.read_csv(data_path+'modified_artists.csv',
        converters={"tags":ast.literal_eval},sep=';',
        encoding='utf-8')
    # removing less popular artists
    df_artists = df_artists[df_artists['listeners'] >= 2000] 
    recommender = Artist_recommender(df_artists)
    profiler = Artist_profiler(df_artists)
    
def get_recommendations(artist_list):
    return recommender.make_recommendations(profiler.create_profile(artist_list))

class RecommendationsHandler(BaseHTTPRequestHandler):
    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def do_GET(self):
        logging.info("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
        self._set_response()
        if self.headers['Content-Length'] is None:
            result_dict = {'Error 1':'Invalid JSON data. Please send a valid JSON data'}
        else:
            data_dict = json.loads(self.rfile.read(int(self.headers['Content-Length'])))
            #self.wfile.write(json.dumps({'abc':123}).encode('utf-8'))
            result_dict = self.compute_request(data_dict)
        self.wfile.write(json.dumps(result_dict).encode('utf-8'))
    
    def compute_request(self,data_dict):
        if type(data_dict) is not dict or 'artists' not in data_dict:
            result_dict = {'Error 2':'Invalid JSON format. Expected a dict with a "artists" key and a list of artists'}
        elif not type(data_dict['artists']) is list:
            result_dict = {'Error 3':'Invalid value(s). Please send a list of artists'}
        elif data_dict['artists'] == []:
            result_dict = {'Error 4':'Empty list. Please send a non empty list'}
        else:
            result_dict = get_recommendations(data_dict['artists'])
        return result_dict

def run(server_class=HTTPServer, handler_class=RecommendationsHandler, port=8080):
    initialize()
    logging.basicConfig(level=logging.INFO)
    httpd = server_class(('127.0.0.1', port), handler_class)
    logging.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')

if __name__ == '__main':
    run()
run()