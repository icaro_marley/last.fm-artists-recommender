# -*- coding: utf-8 -*-
"""
Created on Sat Jun 23 09:11:49 2018

load raw csv dumped from database
do some preprocessing
add domain based tags
creates a new csv file

@author: Icaro
"""
import pandas as pd
import re
import json

data_path = '../Data/'
df_artists = pd.read_csv(data_path+'artists.csv',converters={
    "tags": json.loads,
    'members':json.loads})

df_tags = pd.read_csv(data_path+'tags.csv',sep=';',converters={
        'tag_check':lambda x:x.split(','),
        'tag_add':lambda x:x.split(','),
        })
df_artists['biography'] = df_artists['biography'].astype(str)
def to_float(string):
    string = string.replace(',','')
    try:
        number = float(string)
        mult = ''
    except:
        number,mult = re.match('(.+)([KM])',string).groups()
    number = float(number)
    if 'K' == mult:
        number*=1000
    if 'M' in mult:
        number*=1000000
    return number
df_artists['listeners'] = df_artists['listeners'].apply(to_float)

# info from biography
def get_members(text):
    return [string.strip() for string in 
        re.findall('((?:[A-Z][A-z]+ )+)(?:, \d{2}, )?\((?:guitar|Guitar|,|Lead|lead|Key|key|Bass|bass|Vocal|vocal|Drum|drum)',text)]
df_artists['members'] = df_artists['members'] + df_artists['biography'].apply(get_members)

# add name to members
df_artists['members'] = df_artists['members'] + df_artists['name'].apply(lambda x:[x])
# add sub names to members
join_strings = [
    'feat.',
    'Feat.',
    'featuring',
    '|',
    '&',
    ',',
    'vs.',
    'Vs.',
    'and',]
def get_members_from_name(name):
    members = [name]
    for string in join_strings:
        aux_members = []
        i = 0
        i_non_contain_str_members_list = []
        while i < len(members):
            if string in members[i]:
                aux_members += [member.strip() for member in members[i].split(string)]
            else:
                i_non_contain_str_members_list.append(i)
            i += 1
        if aux_members:
            for i_non_contain_str_members in i_non_contain_str_members_list:
                aux_members += [members[i_non_contain_str_members]]
            members = aux_members
    return members
df_artists['members'] = df_artists['members'] + df_artists['name'].apply(get_members_from_name)
# preprocess found tags
def process_tag(tag):
    tag = tag.lower()
    replace_list = [
        '-',"'",'"',':','#','/','\\','.',',','_',
        '@','|','!','?','[',']','<','>','+','°','}',
        '^','{','º','*',';',
    ]
    for char in replace_list:
        tag = tag.replace(char,' ')     
    tag = tag.replace('&',' and ') 
    tag = tag.replace(' n ',' and ') 
    tag = tag.replace('music','') 
    tag = tag.replace('song','') 
    # c pop k pop j pop etc
    tag =  re.sub('([a-z]+)-?(funk|dance|wop|hop|pop|rock|metal)','\g<1> \g<2>',tag)
    tag = re.sub('^c (.+)','chinese \g<1>',tag)
    tag = re.sub('^k (.+)','korean \g<1>',tag)
    tag = re.sub('^g (.+)','gangsta \g<1>',tag)
    tag = re.sub('^j (.+)','japanese \g<1>',tag)
    # ages 00s -> 00 s
    tag = re.sub('(\d{2})s','\g<1> s',tag)
    # dnb
    tag = tag.replace('dnb','drum and bass') 
    tag = tag.strip()
    return tag
def process_tags(tag_list):
    processed_tag_list = []
    for tag in tag_list:
        processed_tag_list.append(process_tag(tag))
    return processed_tag_list
df_artists['tags'] = df_artists['tags'].apply(process_tags)

# add extra tags
i = 0
def compute_tags(tag_list):
    global i
    found_tags = []
    for tag in tag_list:
        found_tags_aux = []
        for index,tags in df_tags.iterrows():
            for tag_synonym in tags['tag_check']:
                if re.findall(tag_synonym,tag):
                    found_tags_aux += tags['tag_add']
                    break
        found_tags += found_tags_aux
    i += 1
    if i % 10000 == 0:
        print(i)
    return found_tags
df_artists['extra_tags'] = df_artists['tags'].apply(compute_tags)

# add all tags
df_artists['extra_tags'] = df_artists['extra_tags'] + df_artists['tags'] 
# add members
df_artists['extra_tags'] = df_artists['extra_tags'] + df_artists['members']
df_artists['extra_tags'] = df_artists['extra_tags'].apply(lambda x:list(set(x)))

df_artists = df_artists[['name','extra_tags','listeners']]
df_artists.columns = ['name','tags','listeners']
# save modified data
df_artists.to_csv(data_path+'modified_artists.csv',index=False,sep=';',encoding='utf-8')

