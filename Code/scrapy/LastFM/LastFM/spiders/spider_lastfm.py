# -*- coding: utf-8 -*-

import scrapy
from LastFM.items import Artist
import logging
from pymongo import MongoClient
from scrapy.conf import settings

# db pointer
connection = MongoClient(
    settings['MONGODB_SERVER'],
    settings['MONGODB_PORT']
)
db = connection[settings['MONGODB_DB']]
collection = db[settings['MONGODB_COLLECTION']]

   
class ArtistsSpider(scrapy.Spider):
    allowed_domains = ["last.fm"]
    name = "artists"
    start_urls =  [
            'https://www.last.fm/music/Devinho+Novaes',
            'https://www.last.fm/music/Maneva',
            'https://www.last.fm/music/Rise+Against',
            'https://www.last.fm/music/Macklemore',
            'https://www.last.fm/music/Rage+Against+the+Machine',
            'https://www.last.fm/music/Iron+Horse',
            'https://www.last.fm/music/Tenacious+D',
            'https://www.last.fm/music/Jo%C3%A3o+Gabriel',
            'https://www.last.fm/music/Powerwolf',
            'https://www.last.fm/music/Charlie+Brown+JR.',
            'https://www.last.fm/music/Pitty',
            'https://www.last.fm/music/CPM+22',
            'https://www.last.fm/music/Detonautas+Roque+Clube',
            'https://www.last.fm/music/Cordel+do+Fogo+Encantado',
            'https://www.last.fm/music/Elvis+Presley',
            'https://www.last.fm/music/Robb',
            'https://www.last.fm/music/DragonForce',
            'https://www.last.fm/music/Blues+Saraceno',
            'https://www.last.fm/music/Apocalyptica',
            'https://www.last.fm/music/Elvis+Presley',
            'https://www.last.fm/music/Robb',
            'https://www.last.fm/music/DragonForce',
            'https://www.last.fm/music/O+Teatro+M%C3%A1gico',
            'https://www.last.fm/music/Maria+Gad%C3%BA',
            'https://www.last.fm/music/Metallica/',
            'https://www.last.fm/music/Ed+Sheeran',
            'https://www.last.fm/music/Shakira',
            'https://www.last.fm/music/Bruno+&+Marrone',
            'https://www.last.fm/music/Maximum+the+Hormone',
            'https://www.last.fm/music/Rammstein',
            'https://www.last.fm/music/Blind+Guardian',
            'https://www.last.fm/music/Skank',
            'https://www.last.fm/music/RuPaul',
            'https://www.last.fm/music/Beyoncé',
            'https://www.last.fm/music/Richie+Kotzen',
            'https://www.last.fm/music/Ariana+Grande',
            'https://www.last.fm/music/Ana+Victoria',
            'https://www.last.fm/music/Powerwolf',
            'https://www.last.fm/music/Rasputin',
            'https://www.last.fm/music/Favorite+und+Hollywood+Hank',
            'https://www.last.fm/music/Calavera',
            'https://www.last.fm/music/Avicii',
            'https://www.last.fm/music/Imagine+Dragons',
            'https://www.last.fm/music/Barns+Courtney',
            'https://www.last.fm/music/Eminem',
            'https://www.last.fm/music/Kanye+West',
            'https://www.last.fm/music/Guns+N%27+Roses',
            'https://www.last.fm/music/Iggy+Pop',
            'https://www.last.fm/music/Iron+Horse',
            'https://www.last.fm/music/Blues+Saraceno',
            'https://www.last.fm/music/Apocalyptica',
            'https://www.last.fm/music/The+Prodigy',
            'https://www.last.fm/music/Daft+Punk',
            'https://www.last.fm/music/Depeche+Mode',
            'https://www.last.fm/music/Massive+Attack',
            'https://www.last.fm/music/Papa+Roach',
            'https://www.last.fm/music/Johnny+Cash',
            'https://www.last.fm/music/Taylor+Swift',
            'https://www.last.fm/music/Shania+Twain',
            'https://www.last.fm/music/Willie+Nelson',
            'https://www.last.fm/music/Carrie+Underwood',
            'https://www.last.fm/music/Armin+van+Buuren',
            'https://www.last.fm/music/ATB',
            'https://www.last.fm/music/Ti%C3%ABsto',
            'https://www.last.fm/music/Lady+Gaga',
            'https://www.last.fm/music/Madonna',
            'https://www.last.fm/music/Michael+Jackson',
            'https://www.last.fm/music/Megadeth',
            'https://www.last.fm/music/Linkin+Park',
            'https://www.last.fm/music/Pearl+Jam',
            'https://www.last.fm/music/Go',
            'https://www.last.fm/music/Flow',
  ]
    url_base = 'https://www.last.fm'
    artists = {}
    collection = collection

    def parse(self, response):
        c = ArtistsSpider.collection.find({'url':response.url})
        if c.count() == 0: # not in db
            ArtistsSpider.artists[response.url+'/+wiki'] = Artist()
            ArtistsSpider.artists[response.url+'/+wiki']['url'] = response.url
            ArtistsSpider.artists[response.url+'/+wiki']['name'] = response.css('h1.header-title')[0].css('::text').extract_first().strip()
            ArtistsSpider.artists[response.url+'/+wiki']['tags'] = response.css('li.tag *::text').extract()
            try:
                ArtistsSpider.artists[response.url+'/+wiki']['listeners'] = response.css('abbr.intabbr')[1].css('::text').extract_first().strip()
            except Exception as e:
                with open('withoutlisteners.txt','a') as f:
                    f.write(response.url+'\n')
                    raise e
            try:
                ArtistsSpider.artists[response.url+'/+wiki']['birth'] = response.css('p.factbox-summary ::text')[0].extract()
            except:
                logging.info('Birth information not found: {}'.format(response.url))
            yield scrapy.Request(response.url+'/+wiki', callback = self.parse_extra_info)    
        
        i = 1
        try:
            response.css('ol.grid-items')[i]
        except:
            i = 0
        for sel in response.css('ol.grid-items')[i].css('div.grid-items-cover-image'):
            new_url = ArtistsSpider.url_base + sel.css('a')[0].css('a::attr(href)').extract_first()
            yield scrapy.Request(new_url, callback = self.parse)

    def parse_extra_info(self, response):
        biography = response.css('div.wiki-content *::text').extract()
        try:
            members = response.css('li.factbox-item')[-1].css('a ::text').extract() ###
        except:
            logging.info('Members information not found: {}'.format(response.url))
            members = '[]'   
        ArtistsSpider.artists[response.url]['biography'] = ' '.join(biography)
        ArtistsSpider.artists[response.url]['members'] = members
        artist = ArtistsSpider.artists[response.url]
        del ArtistsSpider.artists[response.url]
        ArtistsSpider.collection.insert(dict(artist))
        logging.debug("Artist added to MongoDB database!")   
        yield artist


'''
import pymongo
from pymongo import MongoClient

client = MongoClient('localhost', 27017)

db = client.LastFM
collection = db.artists

#collection.remove()
c = collection.find({'url':'https://www.last.fm/music/Going+Steady'})
for element in c:
    print(element)

c = collection.find({'name':"Mamonas Assassinas"})       
for element in c:
    print(element['members'])

c = collection.find({})
import time
while True:
    print(c.count())
    time.sleep(1)
    
    
c = collection.find({'name':'Macklemore'})    
for element in c:
    print(element['biography'])
'''

'''
SEED
            'https://www.last.fm/music/Devinho+Novaes',
            'https://www.last.fm/music/Maneva',
            'https://www.last.fm/music/Rise+Against',
            'https://www.last.fm/music/Macklemore',
            'https://www.last.fm/music/Rage+Against+the+Machine',
            'https://www.last.fm/music/Iron+Horse',
            'https://www.last.fm/music/Tenacious+D',
            'https://www.last.fm/music/Jo%C3%A3o+Gabriel',
            'https://www.last.fm/music/Powerwolf',
            'https://www.last.fm/music/Charlie+Brown+JR.',
            'https://www.last.fm/music/Pitty',
            'https://www.last.fm/music/CPM+22',
            'https://www.last.fm/music/Detonautas+Roque+Clube',
            'https://www.last.fm/music/Cordel+do+Fogo+Encantado',
            'https://www.last.fm/music/Elvis+Presley',
            'https://www.last.fm/music/Robb',
            'https://www.last.fm/music/DragonForce',
            'https://www.last.fm/music/Blues+Saraceno',
            'https://www.last.fm/music/Apocalyptica',
            'https://www.last.fm/music/Elvis+Presley',
            'https://www.last.fm/music/Robb',
            'https://www.last.fm/music/DragonForce',
            'https://www.last.fm/music/O+Teatro+M%C3%A1gico',
            'https://www.last.fm/music/Maria+Gad%C3%BA',
            'https://www.last.fm/music/Metallica/',
            'https://www.last.fm/music/Ed+Sheeran',
            'https://www.last.fm/music/Shakira',
            'https://www.last.fm/music/Bruno+&+Marrone',
            'https://www.last.fm/music/Maximum+the+Hormone',
            'https://www.last.fm/music/Rammstein',
            'https://www.last.fm/music/Blind+Guardian',
            'https://www.last.fm/music/Skank',
            'https://www.last.fm/music/RuPaul',
            'https://www.last.fm/music/Beyoncé',
            'https://www.last.fm/music/Richie+Kotzen',
            'https://www.last.fm/music/Ariana+Grande',
            'https://www.last.fm/music/Ana+Victoria',
            'https://www.last.fm/music/Powerwolf',
            'https://www.last.fm/music/Rasputin',
            'https://www.last.fm/music/Favorite+und+Hollywood+Hank',
            'https://www.last.fm/music/Calavera',
            'https://www.last.fm/music/Avicii',
            'https://www.last.fm/music/Imagine+Dragons',
            'https://www.last.fm/music/Barns+Courtney',
            'https://www.last.fm/music/Eminem',
            'https://www.last.fm/music/Kanye+West',
            'https://www.last.fm/music/Guns+N%27+Roses',
            'https://www.last.fm/music/Iggy+Pop',
            'https://www.last.fm/music/Iron+Horse',
            'https://www.last.fm/music/Blues+Saraceno',
            'https://www.last.fm/music/Apocalyptica',
            'https://www.last.fm/music/The+Prodigy',
            'https://www.last.fm/music/Daft+Punk',
            'https://www.last.fm/music/Depeche+Mode',
            'https://www.last.fm/music/Massive+Attack',
            'https://www.last.fm/music/Papa+Roach',
            'https://www.last.fm/music/Johnny+Cash',
            'https://www.last.fm/music/Taylor+Swift',
            'https://www.last.fm/music/Shania+Twain',
            'https://www.last.fm/music/Willie+Nelson',
            'https://www.last.fm/music/Carrie+Underwood',
            'https://www.last.fm/music/Armin+van+Buuren',
            'https://www.last.fm/music/ATB',
            'https://www.last.fm/music/Ti%C3%ABsto',
            'https://www.last.fm/music/Lady+Gaga',
            'https://www.last.fm/music/Madonna',
            'https://www.last.fm/music/Michael+Jackson',
            'https://www.last.fm/music/Megadeth',
            'https://www.last.fm/music/Linkin+Park',
            'https://www.last.fm/music/Pearl+Jam',
            'https://www.last.fm/music/Go',
            'https://www.last.fm/music/Flow',
'''