# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

from pymongo import MongoClient
import logging
from scrapy.conf import settings
from scrapy.exceptions import DropItem

class MongoDBPipeline(object):

    def __init__(self):
        connection = MongoClient(
            settings['MONGODB_SERVER'],
            settings['MONGODB_PORT']
        )
        db = connection[settings['MONGODB_DB']]
        self.collection = db[settings['MONGODB_COLLECTION']]

    def process_item(self, item, spider):

        valid = True
        for data in item:
            if not data:
                valid = False
                raise DropItem("Missing {0}!".format(data))    
        if valid:
            '''
            # check if collection have the item
            if self.collection.find({'url':item['url']}).count() > 0:
                logging.debug("Artist already collected")
            else:
                self.collection.insert(dict(item))
                logging.debug("Artist added to MongoDB database!")
            '''
            self.collection.insert(dict(item))
            logging.debug("Artist added to MongoDB database!")   
        return item
 
'''
import pymongo
from pymongo import MongoClient

client = MongoClient('localhost', 27017)

db = client.LastFM
collection = db.artists

#collection.remove()
c = collection.find({'url':'https://www.last.fm/music/Going+Steady'})
for element in c:
    print(element)

c = collection.find({'name':'Alestorm'})       
for element in c:
    print(element['members'])

c = collection.find({})
import time
while True:
    print(c.count())
    time.sleep(1)
    
    
c = collection.find({'name':'Three Days Grace'})    
for element in c:
    print(element['biography'])
'''