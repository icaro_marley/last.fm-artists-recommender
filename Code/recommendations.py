# -*- coding: utf-8 -*-
import pandas as pd

class Profile():
    def __init__(self,liked, tags):
        self.liked = liked
        self.tags = tags

class Artist_profiler():
    def __init__(self,artist_data):
        self.artist_data = artist_data
    
    def create_profile(self,artist_list):
        tag_list = []
        for artist in artist_list:
            if artist not in self.artist_data['name'].values:
                print('A artist was not found',artist)
                continue
            tag_list += self.artist_data[self.artist_data['name']==artist]['tags'].values[0]
        if len(tag_list) == 0:
            print('Artist(s) not found.')
            return None
        tag_dict = {}
        for tag in tag_list:
            if tag in tag_dict:
                tag_dict[tag] += 1
            else:
                tag_dict[tag] = 1
        profile = Profile(artist_list,tag_dict)
        return profile        
        
class Artist_recommender():
    def __init__(self,data):
        self.data = data

    def make_recommendations(self,profile):
        artist_data = self.data[~self.data['name'].isin(profile.liked)]
        artist_names = artist_data['name'].values
        df_similarity = pd.DataFrame([],columns=artist_names)
        similarity_list = []
        for _, artist in artist_data.iterrows():
            similarity_list.append(self._compute_similarity(profile,artist['tags']))
        df_similarity.loc[0] = similarity_list
        df_similarity.loc[1] = artist_data['listeners'].values
        df_similarity = df_similarity.transpose().sort_values(by=[0,1],ascending=[False,False])
        df_similarity.columns = ['similarity','listeners']
        df_similarity['similarity'] = df_similarity['similarity']
        return {'Recommendations':df_similarity[:5].index.values.tolist()}
        #return df_similarity[:10].to_dict()

    def _compute_similarity(self,profile,tag_list):
        similarity = 0
        for tag in tag_list:
            if tag not in profile.tags:
                continue
            similarity += profile.tags[tag]
        return similarity
     
'''
import pandas as pd
import ast

data_path = '../Data/'
df_artists = pd.read_csv(data_path+'modified_artists.csv',
        converters={"tags":ast.literal_eval},sep=';',
        encoding='utf-8')

profiler = Artist_profiler(df_artists)
artist_names = [
'Avantasia',
'Imagine Dragons',
'Childish Gamgino',
'Linkin Park',
'Shawn Mendes',        
'Blues Saraceno',
]

artist_names = [
'Avantasia',
'Imagine Dragons',
'Childish Gambino',
'Linkin Park',
]
artist_names = ['Welshly Arms','Blues Saraceno','The Silent Comedy','Johnny Cash']
artist_names = ['Skank','Capital Inicial','Charlie Brown JR.']
artist_names = ['Os Paralamas Do Sucesso','Titãs','Legião Urbana','Rita Lee','Barão Vermelho']
artist_names = ['Justin Timberlake','Tiago Iorc','Ed Sheeran','Ariana Grande']

artist_names = [
'Jonas Esticado',
'Devinho Novaes',
'Maneva'        ,
]

recommender = Artist_recommender(df_artists)
profile = profiler.create_profile(artist_names)
profile.tags
print(recommender.make_recommendations(profile))


list(df_artists[df_artists['name'] == 'Michael Schenker']['tags'])
'''