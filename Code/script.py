# -*- coding: utf-8 -*-
"""
Created on Fri Jun 22 19:41:48 2018

@author: Icaro
"""

import pandas as pd
import numpy as np
import re

data_path = '../Data/'

df_artists = pd.read_csv(data_path + 'artists.dat',sep='\t')
df_tags =  pd.read_csv(data_path + 'tags.dat',sep='\t',encoding='latin-1')
df_user_artists = pd.read_csv(data_path + 'user_artists.dat',sep='\t')
df_user_tag_artists = pd.read_csv(data_path + 'user_taggedartists.dat',sep='\t')
'''
# processed dfs
df_user_tag_names_artists = df_user_tag_artists.merge(df_tags,on='tagID')

df_user_tag_names_artists['tagValue'] = df_user_tag_names_artists['tagValue'] + '|'
df_artists_tags = df_user_tag_names_artists.groupby('artistID')['tagValue'].sum().reset_index()
df_artists_tags = df_artists_tags.merge(df_artists,left_on='artistID',right_on='id')
df_artists_tags.drop('id',axis=1,inplace=True)
#df_user_tag_artists = df_artists_tags.merge(df_user_artists,on='artistID')

# compute similarity
df_artists_tags['tagValue'] = df_artists_tags['tagValue'].apply(lambda x:x.rstrip('|')).apply(lambda x:set(x.split('|')))

# return similarity between artists
def compute_artist_similarity(artist1_row,artist2_row):
    tags1 = artist1_row['tagValue']
    tags2 = artist2_row['tagValue']
    return len(tags1.intersection(tags2))
# check similarity between artists
def check_artist_similarity(artist_name):
    if artist_name not in df_artists_tags['name'].values:
        print("artist not found")
        return None
    df_similarity = pd.DataFrame([],columns=df_artists_tags['name'].values)
    artist1 = df_artists_tags.loc[df_artists_tags['name'] == artist_name].iloc[0]
    similarity_list = []
    for _, artist2 in df_artists_tags.iterrows():
        similarity_list.append(compute_artist_similarity(artist1,artist2))
    df_similarity.loc[0] = similarity_list
    df_similarity = df_similarity.transpose().sort_values(by=0,ascending=False)
    df_similarity.columns = ['similarity']
    df_similarity['similarity'] = df_similarity['similarity'] / df_similarity['similarity'].iloc[0]
    df_similarity.drop(df_similarity.index[0],inplace=True)
    return df_similarity
check = check_artist_similarity('Ana Victoria')
'''


# tag frequency
'''
min_freq = 100
df_user_tag_names_artists = df_user_tag_artists.merge(df_tags,on='tagID')
tag_freqs = df_user_tag_names_artists['tagValue'].value_counts(dropna=False,ascending=True)
check_tag_freqs = tag_freqs[tag_freqs <= min_freq].reset_index()
check_tag_freqs['tagName'] =  check_tag_freqs['index']
len(check_tag_freqs)
'''


# local
swedish 
mpb
europeia
brasileira
sweden
sounds like whale from sweden
# type
gospel
# artist
beyonce
the best of madonna
# age


#instrumental


roxette
joyride
scandinavian
christmas songs
gossip girl
salsa
trumpet
colombia
reggaeton
happy
vivaldi
salsa romantica
vocal
rock en espanol
concerto
love at first listen
barroco
antonio vivaldi
barroco favorito
latino
70s
dance rock
great lyrics
nu-metal

brutal deathgrind
grindcore
brutal death metal
technical death metal
death metalcore
brutal deathcore
progressivo
christian metalcore
jpdm
deathgrind
post hardcore
russian death metal
bree bree
goregrind
vocalist
bay area
southern rap
indie folk
instrumental hip-hop
acoustic music
rock in rool
pop metal
not emo
andy six
post-hadcore
lady gaga play day
holiday soundtrack
unreleased
stand out be proud
demi lovato
my childhood
underrated
one of her best songs
her best
amazing voice
elvis presley
gross meaning but good song
why on earth wasnt this released
party time
vanessa hudgens

hilarious
american idol
never gets old
celebrities who one day im determined to meet
nick
wishlist

soundrack of my life
taylor swift

play this loud enough to raise the dead
haunting
music for tossing furniture around
cello 
hypnotic

apocalyptic
so sad yet so beautiful
i think trent reznor is superior to god
peaceful
great b-side
my face is completely rocked off
fucking beautiful
mega awesome sweet fuckin hyperpowerful riff
greatest rock anthems
my music pillow
chen-
post rock
basque
brasil
mellow
relaxing
chill
medieval
finnish
blues
guitar
the best
music to cry
energy
ukrainian
nostalgia
dreamy
sad and slow
amazing vocal
sad
perfect lyrics
absolutely genius
piano rock
rock ballad
sensual
meditative
placebo
female vocal
hair metal
riot grrrl
cover
rock catala
christian metal
celtic
spiritual
new rave
mixmaster supreme
disco classic
smooth groove
damn boy
glorious gospel
deep house mixes
90s jam
forgotten 80s jam
loleeta
memphis soul
miss grace jones
grace
loleatta
drum 'n' bass
dnb
hot enough to sweat out your press n curl
quiet storm
70s jazz

sad 
love
fonky fresh jam
undulating
slinky
debut

david at his finest
luther vandross
philly soul
mariah carey
leona lewis
arjen anthony lucassen

instrumentals
pure aural sex
sunny
lo-fi

weird
clocks

space 
metroid
duke nukem
game
my other car is a cdr
lisp
pirate 
alliterative


sicp

june

the beowulfs choice
scholes song
the pedofinder general
po-kraftwerkski
zadrotstvo
my fav fun
ogonj
oni pout
ohm sweet ohm
nejlepsi
po-andoneovski
hellish song
polotenchik poet
my choice
my fav voice
tango
my calm
true synthpop
crossover
andrew lloyd webber
tenor
mezzo-soprano
pop opera
soprano
baritone
popera
alessandro safina
caruso
the phantom of the opera
duet
turandot
musical
adagio
phantom of the opera
foll
mepoupe
korean
pure love
somebody told me
alternatif
anglais
darkstep
darkside
hardstep
belarus
death-metal
mathcore
g-funk
bandana music
westcoast
g-funk francais
bay rap
chicano rap
laglak
gangsta rnb
pimp rap
jazz rock
math rock
jazzcore
jazz core
indie jazz dance math rock
aysel gurel
onno tunc
sezen aksu
perdesiz gitar
banu kirbag
garo mafyan
atilla ozdemiroglu
arabesk
erkan ogur
gundogarken
bir yaz daha bitiyor
istanbul
baha boduroglu
turk sanat muzigi
mozaik
cook alametler belirdi
nur yoldas
parios
flamenco jazz
modern klasik
ezginin gunlugu
yildirim turker
oludeniz
tanju duru
selmi andak
sn

vedat sakman
arto tuncboyaciyan
kivanc k

rock 'n' roll
backstage rock shop artist
loistavuus
rhythm and blues
scandinavian action rock
rock n roll
rock'n roll
seattle sound
trancecore
mexico
favourite albums
gader
dirty south

female voices
justin timberlake
jessica simpson
songwriter

inspirational
jacksons
fmera album
best song in the world
lofi
chill zone
covers
miserable
christmas blend
rain
morning
duets
industri
steve albini
steve abini
no wave
avant garde
marathon
beer
touhou
hai
susu
utage
peppy
video
nico
iosys
osu
ievan polka
ryusueigun
can
you
just
gaga
parody
power
polka
spice girls
backstreet
hair
afro
parappa
cut
trim
flave
bass
hoppar
mafia
superhit
masz do cholery
musicals
ross
damon albarn
drone doom
alternativemetal
hott
sufoco
80's music
big fun
big hair
supercool
deep
classic pop
heavy funk
tecno comedy
80z pop
tragically-80s
popdance
hillarious
hot track
funny scottish bastard
sexy song
positive
beautiful sounds
dance funk
gorgeous boys
gorgeous soul
dancerock
mosh song
hot voice
fun-dance-song
great love song
popfun
australian rock
genius at work - brilliant song
aussie pop dance band
80z rock
eerily beautiful
poprock
hotttttttt
brilliant live rock band
aweosme rock
british tosser
ahhhhhhhhh
australian pop rock at its best
timeless rock
lush
wow
exclamation mark
dreampop
cheese on toast
samey
just a little bit sexy
wooooooaaaaahh
jewish folk alternative rap
hilary duff
featuring
slash
fergie
8-bit
overrated
southern rock
synth indie rock
favourite artists
simple but awesome
i love this fucking song
so catchy i might explode
depeche mode covers
marisa mix
ben folds
tom waits

solo
brawler
bluesy
loneliness
green day
the weakerthans
the roots
regina spektor
horns
iron & wine
cake
randy newman
growing up
alternative music
alternatif muzik
rock gaucho
rio grande do sul
powerpop
rock porto alegre
italian pop
christian
road  movie
disney on ice
hal
instructive
audio book
propaganda
core
school
etheral
chipgaze
experimental metal
breakcore
speedcore
chiptune
raggacore
jungle
8bit
terror
8 bit
latvian
breakbeat
hardcore techno
south park
cartman
dominican republic
crossover
old school death metal
ambient black metal
funeral doom metal
funeral doom
death/black metal
sleaze rock
glam-sleaze rock
glam metal
rock 80s
street punk
chaos punk
sleaze
performer
best seller
entertainer
pop icon
pop queen
superstar
grammy winner
recordwoman
very very guilty pleasure
britney fan
heartbreaker
composer
mtv
unreal
ost
record
bleach
star
tears
big bang
living legend
unusual you like
blackout
truth
bleachaddict
jpop
emotion
transcendant
pop princess
amazing fans
best song
post-metal
grime
artcore
jumpstyle
unique song
war is like music
dub dos brother
fucking awensome
pink floyd
guns n roses
fucking cool
american blues artist group
ameribluesscene
blueartistgroup
american blues scene
guitar karlsruhe
allboutguitar
seattle
blues music
red shoe diaries
brent david fraser
fame la
harmonica blues
dudley taft
german blues lovers
excellence
great song
great intro here nice chill beat with uptempo feel as well
one of the illest groups dah shinin is a must get top 3 album ever
vocal trance
love songs
scottish
soft rock
christmas
50s
eurodance
post-grunge
modern metal
german metal
industrial death metal
stoner metal
sludge metal
bulgarian
female fronted metal
freestyle
shred
funny
radio show
advice
talk show
folk metal
viking metal
wolf metal
carrie underwood
divas
carrie underwood 
sex pop
adam lambert
lil wayne

my favorite artists
female singers
country albums to check out
eminem
linda perry
im ok
kelly clarkson
already gone
pop rock nacional
philadelphia
country dobro americana dylan humor parody satire funny zappa
fiscal crisis
relevant now
acme of western civilization
fingerpicking
tromboners
godlike
eceletic
philadelphia philly funny quirky
phildelphia
ents
west philly
fugs
upper darby
trees
talking heads
weed
devo
phish
marijuana
zappa
philly
420
trippy
humor
drugs
prog
quirky
aufdiefresse
urz
family guy
big beat
aggressive
humour
love metal
the beatles
fevereirooooo lol
music faz lembrar a a nani num sei pq mais eu lembro
music gay :d
music ki me faz lembrar meu amor s2
digimon xd lol
music lembra quando eu thava 7 serie lol
lol music boa de mais da conta
rock ballads
finnish rock
theatrical
emotional
dramatic
remix
educational music
kindie
childrens music
prog-rock
coffeehouse
beatles influenced
beck-influenced
dancehall
song for the dumped
chill-out
orchestral pop
horror film music
theatre
movie music
broadway
eerie
amy macdonald
love is weird
can not have the best of me
female vocalits
roots reggae
minimal techno
electrodub
loose
nelly
furtado
axe
forro
whitneyduncan
kelly key
goth
brazilian post-punk
indie funk
dark pop
dark metal
girl band
portuguese
future-pop
french pop
electropunk
synth-rock
fav female singers
fav tracks
fav male singers
relax
good mood
depressing
sex
abre caminhos
minuto de sabedoria
stupid
luz luz luz
worst lyrics ever
turkish delight
language
best version
adoro
preferidas
brasileiro
female singer-songwriters
jack johnson
soundscape
shit
meow
repetitive
drill n bass
breathtaking
trash metal
symphonic black metal
pagan metal
indietronica
rock'n'roll
synthrock
fusion jazz
art rock
melodic hardcore
burnt fur
siouxsie and the banshees
the cure
paradise place
the human league
seconds
ultraviolence
new order
clasica
70
seventies
setenta
setentas
rb
romantica
noruego
nordico
the nordic sound
chilena
illapu
books
folklore
filmmussik
pelicula
spoken
espana
tranquility
trompeta
relajacion
dreaming
israel
jerusalem
la nueva ola chilena
classicalclassic
nueva ola
vieja ola
bavarica
queen of rnb
queen of hip hop
mother monster
sasha fierce
bootylicious
prince of pop
cute smile
gold pants
beautiful eyes
best dancer
curly hair
king of music
love his voice
childhood hero
peter pan
oops she did it again
popular
filipino
champion
singer
folktronic
ellie gouldin
philippines
american band
onerepublic
the fray
philippine
pinoy
woman beater
douchebag
asshole
bizarro
brazilian pop
dragmusic
better than radiohead
eletronic pop
bitch
best remix ever
bewareofthedog
digital hardcore
cyber-punk
techno punk
cyber-grind
faggot
speedgore
lolicore
gabber punk
digital-punk
digital-grindcore
fl band
bossa nova
russian pop
haiti
black music
international singers
mariah
ne-yo
modern country
rem
lady antebellum
beyonce knowles
robbie williams
michael
samba
alternativo
excellent reason for crying
these guys fucking rule
fuck yeah
but the party dont stop
just makes you feel good
for days when i am particularly angry with you
fuck u bitch
songs i can actually listen to on repeat
makes you wanna dance all around your room
boys
we should be together
pop bitch
i was fighting with myself not to listen to it but i lost
bitch song
should have been on the album
make me wanna scream
i wish i could make a video for this
i have a definite favorite part in this song
i can and will listen to this song all day
love him
i hate her but i love the song
for haters
live track
amazing cover
tracks i would put on a movie soundtrack
will you marry me sir
its britney bitch
edward and bella
divorced kids anthem
summer song
i love him
femme fatale
lets make a sandwich
to hear when you wake up
broken family
i could imagine hilary duff doing this song
in the middle of summer
summer night
faves
dark-doom
technical metal
new metal
horrorpunk
batcave
this isnt wild
it just sucks
voodoo
a reason the surfaris should only do instrumental songs
the beach boys are pussies
stop singing dick
spooky funk
mourning for grasshoppers
western deathrock
gothic calliope
poncy goth
music that has made me shout fuck yes
aw man i got the goo goo itch
jagger and bowie boned and you know that bowie was the girl because hes bowie
god damn it on days like this i just have to put bela on loud and shut my eyes until the universe stops
i could tell from the guitar that this is serious rock and roll
ska that isnt for annoying 14-year-olds in orange billabong shirts
welcome to drinkin island
pumpkin fiction
dick fucking dale motherfucker yes
i love men without hats but this album is more like grunge without balls
this singer is way too white to cover cab calloway
i take it back
thailand street punk
one of my first favourite albums from the just-discovering-music-times
horror glam
cartoon metal
monster sleaze
outlaw country
love and death
synthmope
i am going to dig up and be indecent with wendy o
comedy folk goth
old devo
which means way more punk than anyone
dusty dark western gothic rock
hard
ass-kicking southern sleaze
deathglam
cowpunk
go back to kokomo you fucking beach boy assholes
glam
arctic monkeys
deep purple family
save your kisses
crystal kay
taboo
utada
neo-soul
club
krautrock
tribal
beautiful female vocalist
trance metal
vampirefreaks
perfect woman
gothic goddess
worlds most beautiful woman
goddess
christ complex
christian black metal
unblack metal
the speed of awesome
deserve to be more famous but would suck if they became mainstream
greatest band in the universe
dragonball z
cute
male vocalist
youtube
made at home
cartoon
cruj
draco and the malfoys
skinny
cindy lauper
queer
the best place to live
aaron carter
victoria beckham
nick carter
country ladies
australian country
canadian country
texas country
kickass
chilling
absofuckinglutely insane
hatris
so good
incredible
burnap
chocolate
downright beautiful
ear sex
fucking hardcore
demos
singer/songwriter
fresh prince
bus stop
retro
great vocal
awesome album
obama
xmas perfection
californication
smooth jazz
powerful
alkohol
kick-ass
cap
post-dubstep
nnnnnnnnnnnnnnnnnnnnnnnnnnnn
lil b house
thejunner
godmother of soul punk
add
paint
picasso
this is paint
virginal power electronics
enough bbq now your presence is needed in albatross
noise pop
ugh
david cameron
cuntservatives
shih tzu
camera
heat
narrative
hearts
submission
ben
fuck ben
benladen
sex sex sex
sex sex sex sex sex
sex sex sex sex sex sex sex sex sex sex sex sex sex sex sex
sex sex
sex sex sex sex sex sex
sex sex sex sex
uk garage
r and s
ninja tune
nu-jazz
beats
electronic rock
triangle
math metal
russian alternative
mashup
horror punk
chillwave
manchester
my fav
orgasm
roots
steve forrest
groove armada
alt
black light
californian
honky tonk
mgmt
england
post punk
female and male vocalists
album i own
andrew vanwyngarden
ben goldwasser
leave you far behind
my space
boy loves guitar
chills
soul rock
rainy day
funky
favorite country music female
favorite country music group
favorite country music male
powerhouse voice
general hospital
favorite country duo
former lead singer of lonestar
christan
seattle band
one tree hill
broadway wicked
favorite song
soap opera
theme song
little mermaid broadway
fucking amazing
intense
p-funk
progressive death metal
two-tone
brilliance
funk rock
tracks that own
epic metal
current song im learning
jazz funk
black folk metal
best maiden track ever
melodeath
essential version
reminds me of my childhood
interesting
professional
intergalactic space pope
raging
my brain hurts
understandable lack of trousers
grandmaaaaaaaaaa
hail to the king
not nsbm
cool
great voice
troiacore
negativo
anti-school
norwegian ambient
japanese rap
cicciputaiutacitu
artedivinatoria
wanda
eoni
erba gramigna
bestiale
ti devi spaventare
infernale
download required
takes my breath away
music of my childhood
dowloan required
nitin sawhney
voice of italia
poslushat
soft
music icon track
perfect sunday evening
love this track more than anything else
tired of being sexy
sweet pornographic track
dave jumps in crowd background females sing louder
depeche mode live black celebration
twin peaks
twin peaks remix album
poetry
subliminal
cosmogonic
abyss
past tense
freak voice
italodisco
dazs favourites
70's
00's
daz cds
brum
90's
demo
daz reggae
ronnie james dio
lock up the wolves
simon wright
teddy cook
game music
skyroads
bouncy
dance music
all time hit
sing along
male and female
dance 90s
male
euro 90s
dance 00s
house music
euroreggae
90s dance
made in brazil
italodance
ragga
slow
scream along
x-mas
eletronica
manson
marilyn manson
shockrock
progressive deathcore
progressive metalcore
technical metalcore
technical deathcore
mix
svenska
sega
amazing mix
techno-ish
icelandic
ampop
psychedelic-pop
psychedelic-folk
neo-psychedelic
psychedelia
60s-psychedelic
60s-psychedelia
socal
no doubt
weezer
duran duran
paramore
the strokes
muse
coldplay
led zeppelin
rock band
brutal
linda
sensacional
rap infantil
lacta
rock-n-roll
porto alegre
acustico
melodic metalcore
bode music
baka
brazilian metal
calm
the battle of maldon
sharon den adel
claire voyant
my 500 essential songs
eses muleq eh da pesada
melhor que cine
reggae finlandes
pagode noruegues
made of fucking win
dookaralho
awesome song
better than paramore
tr00 metal 666 from hell
tr00ta
nem existe mais
beeshona
foda
bubblegum
the only true metal of steel
good pop
xxt
jewish metal
sou foda
covers that better than original
de faveladooo qqq
som de pretooo
gothenburg metal
hundycore
progressive hardcore
pop-punk
no idea
flint
michigan
sub-pop
no fi
low fi
alt country
alt-country
sing-a-long
new york dolls
ireland
irish
horror
pre-punk
cyber-grunge
sublimation
great album
siberian sound
new prog
cybercore
kirov metal fail
ya by vdul
punk77
marc anthony aguanile
p!nk
prosto sanych
oaxaca
mena
warped tour
hair style
depressive black metal
suicidal black metal
dsbm
depressive suicidal black metal
true black metal
norwegian black metal
german thrash metal
death n roll
blackened thrash metal
blackened death metal
rock female
vocalists
circus
jesse mccartney
enigmatic
loveliness
magic
dave gahan
awesome rock
portugal
better than beatles
rei do universo
the shit
carmen sclafani
wiser time
70's rock
slide guitar
there and back again
acoustic rock
all for one
beggars and thieves
americana
love and devotion
jam rock
alt rock
retro rock
chill music
southern soul
bands i have seen live
hard blues
newjersey
alternative country rock
southern rock revival
wiser time music
rhythym blues
show some love
holiday
holiday music
christmas music
garbage city radio
i am canadian
first-rate and free
halloween
rock hard
my indie friends
songs for gil
my angular favourites
songs for sssi
lungbutter
female vocalists i like
actual lungbutter
the music maker society
satire
they might be giants
merzbow
porno
porn
surf punk
hall
frank zappa
weird al yankovic
tenacious d
weird al
porno rock
secret chiefs 3
sun city girls
gang gang dance
twisted
the jesus lizard
eclectic
liars
deerhoof
captain beefheart
wolf eyes
the residents
beautiful genius
alternrock
twin cities
the better of us
five days off
bage
poa
before
how
banda
bones
underground
odd
music
band
each
pagode
brazilian country
sertanejo
college rock
album rock
new zealand rock
blue-eyed soul
jangle pop
new discovery
alternative dance
pub rock
british punk
adult contemporary
american punk
pop underground
american underground
experimental rock
new york punk
folk-rock
neo-psychedelia
british invasion
arena rock
roots rock
chaotic hardcore
beatdown hardcore
beatdown
powerviolence
russian rock
noisecore
straight edge
moshcore
ratedd
heart

biggest-voices
the place promised in our early days
thebest
korean music
burlesque
all-time favourites
blonde
pure
kpop dance
traditional country
folk - folkrock - altcountry
twee
country gospel
twee pop
old time country
british folk
progressive folk
cabaret
carter family
cowboy
old time
folk blues
space rock
contemporary folk
underground hip-hop
british music
thai
beyond the sea
k
pianist
beaufiful eye
jester
the greatest
liza

doda
elektroda
katharsis
dodafanpl
dorota
laska
female focalists
best of
dober2
poland
polska
shemale vocalist
play back
whip my hair
se joga na balada
progressive house
dark psytrance
breaks
slovenian
new wave of british heavy metal
classic metal
chris brown
bow wow
italy
megasound
italia
roma
italiana
zach galifianakis
a filthy lesson for lovers
riccardo sabetti
totec radio
dark rock
neo
funkrevolt
bologna
tribraco
argine
music and miles
antonio zitarelli
squartet
goodphellas
carlo conti
steve piccolo
zu
manlio maresca
strumentale
brass band
maurizio catania
gabriele caporuscio
avantgarde
john zorn
thrangh
gianluca ferrante
valentina d-accardi
mr. bungle
30 seconds to mars
depressive rock
transexual
fea
perra
travesti
naca
gata
gorda
anarcumbia
malerrimo
malisima
don nadie
fracasada
dientona
muertadehambre
enana
electro-pop
elecronic rock
samba jazz
favorite artist
keri hilson
rockfull
rockpure
modern rock
new
upcoming  hit
leighton meester
sweet
kopie dupsko
groove coverage
always in my mind
best of 10
smutne pipczenie
zmiel pierogi
jebane techno
xoxo
melodic trance
uplifting trance
uplifting
electro-house
dream
ultimate rock ballads
grim and necro
true and cult
rock sujo
mod rock foda
rock brasilis foda
rock brasilis
rockabilly sensual
gorgeous
girl group
foreign
girl bands
comic
young
european
roxanne emery
jessie j
favorite artists
dj
sampling
samples
downbeat
abstract hip-hop
abstract hip hop
instrumental hip hop
ambient breakbeat
dj shadow
djs
producer
alternative ambient
other
princess
eua
famale
hardcore melodico
pop music
wanessa camargo
electrropop
club mix
dancefloor
miley
tvorchestvo oligofrenov
gayrock
brutal anal gotik metal
gypsy
emoviolence
hevi
kircore
pepsi
the lion king
pirates of the caribbean
this is it
rio
king
attila
partycore
melodic deathcore
technocore
kid cudi
metalcore metal
and hell followed with
atl
local
system of a down
straight edge hardcore
whitechapel
breakdown
good
the saint alvia ca
sexy rock
brutal grind
elio
animal grindcore
roba che non so se mi piace
dammi una lametta che mi taglio le vene
cold case soundtrack
asher book
tommy february6
tommy heavenly6
tomoko kawase
a-ha
only the young
episode 46
mamoru
usagi
sailor moon
twilight original soundtrack
inuyasha
cold case original soundtrack
the most beautiful song
ghosts of girlfriends past
lovel
the best score ever
greatest hits
lebanese
deep house
brutal kircore
brutal death
nirvana
deftones
gold
oasis
drums
keane
mysterious
old computer game
radiohead
risunki
queen b
makes me smile
fucking hot
futura
electro punk
rap core
wanessa
fael
drum n bass
neurofunk
techstep
drums n bass
menacing
darkcore
liquid funk
dark drum n bass
nintendo
game soundtracks
video game music
pure aural bliss
san francisco
basssss
mmmm
shivers
thrash
huindie
hevimetal
midi-industrial
sunska
ferrumka
black plastic
brutal anal vocal
hot stuff
girly
cybergrind
synthcore
discocore
experimental deathcore
electronic deathcore
meinhundcore
crazy
modern
leave me alone
trip
love that
viagem
outside
fuck you
goodbye
best songs ever
progressive adrian
level two
level one
1st  vine
nu jazz
funky house
level three bitches
omfg
1st vine
soulful house
turntablism
cafe del mar music
chill latin hh
free jazz
french house
sexrhythmic
toque
fractured
visions
fractured visions
burnside
birstall
vision
matt
leeds
this
ship
heavy
yorkshire
wall
danny
fractured vision
west
this ship
batley
post
away
washed
bassist
indie acoustic
roock
fgth
remake
remixes
tribute bands
nkob
former white boi rappers
mark wahlberg
marky mark
nkotb
power ballad
novelty songs
obscure
after-wham
euro-pop
belinda carlisle
ivory queen of soul
erotic
marc almond
cross-over artists
move your body
wait and see
i love being a girl
camryn
wait and see remix
forever and always
taylor swift speak now fearless
psychedelic prog
psychedelic space rock
health food faggot
unknown
the perfect band
happy guys
heaver than beethoven softer than bieber
tomfoolery
billy jack
stoner music
a band for girls and pussies
forestpsy
ambient dub
my vinyl records
shock rock
favourite tracks
sixties
fifties
japan
freak folk
new weird america
black
snakes on a motherfucking plane
vocal jazz
rocj gaucho
junkie
tarja
something great
lovely voice as usual
whitesnake cover
nothing
personal jesus
little 15
new dress
lillian
policy of truth
world in my eyes
progressive psytrance
natural trance
full on trance
progressive electro house
old skul electronic
web
homeboy
slim shady
god
boybands
cheryl cole
beast
harris
calvin
i fancy him
solo
inspiring
nicola robert
funniest guy ever
empowering
pure gold
materpiece
queen of music
not afraid
smash
masterpieces
girlgroups
cheesy
pure pop
rock alternative
johnnys entertainment
hello project
groovy
arashi
cute music
j-urban
reggeaton
japanese pop
news
danceable
rock steady baby
fashion
johnnys
seamo
best motherfucking song ever
namie amuro
dancing queen
most beautiful voices
fujimoto miki
morning musume
kawaii
giza
rina aiuchi
love the lyrics
beloved
clazziquai project
korean male solo artists
musicians with extremely sexy voices
iron maiden
bruce dickinson
sandy leah
gwen stefani
michael jackson
nintendocore
death punk
italian rock
rock italiano
pop italiano
cantautori
cantautore
perfect song
best
poet
my life
minha
my drug
you make me smile
special for me
eighth wonder of the world
amo-te
brazil loves shakira
brilliant cantautor
orgulho latino
all time faves
dance around your bedroom
heartbreak
5 star
dance all night
best she wolf song
viciante
best coreography
talented
zoocore
hardcore e straight edge
beardcore
brincando de ser taking back sunday
vergonhoso
favorite songs
qt
their old stuff was good but they suck now
hanson
tulsa
sacred fools
check them out
charity
favorite albums
more of this please
ozzy osbourne
nancy wilson
ann wilson
alice in chains
randy rhoads
neo classical
axl rose
fretboard frenzy
japanese metal
kelly simonz
southern fried metal
british metal
kick ass candian rock
awesome cover song
metal icon
polital message
fantastic
sebastian bach
alice cooper
neon synthesis
ace frehley
powerman 5000
epica
scorpions
addicted to metal
kick ass
sports
duff mckagen
izzy stradlin
acoustic guitar
vintage
anthem
ozzfest
jake e lee
april wine
headbangers ball
thrash metal at  its finest
ray charles
ray
two and a half man
hipster-hop
father of metal
underrated essentials
black sabbath
french metal
toxxic toyz
france
nice
fisc
ump
optic 2000
suisse
varietoche
collabo
traditional doom metal
neoclassical
tbm
martial industrial
argentina
old school goth
melancholic idm
folk-lore
military ebm
dark folk
experimantal
solo piano
bsb
single
divas
carter
forever in heart
rock nacional
funy
backstreet boys
varius manx
peru
peruvian
romania
quechua
cusco
chicha
huayno
runasimi
technocumbia
peru rock
arequipa
violin
manele
peruvian rock
andes
chicha psicodelica
death brutal technical atmospheric metal
cumbia
runa simi
musica docta
spain
lima
moldova
andean
viola
malasia
rock peru
cumbia andina
ecuador
moldavia
bollywood
calla cachera
national treasure
impassioned
unique
exciting
original
better than lady gaga
the one who turned out much saner than that other one
real talent
the queen
cheetos queen
lonely popstar
stunning
i wanna dance like that
pop heaven
gay heaven
sold more than blackout in the uk
surf music
idolos
tropicália
rock progressivo
pop singer aguilera xtina eletro minaj
spears
tv
britney
vh1
paparazzi
celebrity eye candy
best ost
indie-rock
bobim
putaria sound
awesome singer

rebellious
indie punk

punk-rock
the king
damien
musica para fazer sexo
space
psybient
energetic
sxe
fallout
amazing songs
ambient lounge
psychill
angeles del infiernos
baron rojo
rata blanca
denim
mago de oz
venom
manowar
leather
fast
headbangers
judas priest
motorhead
thrashers
heathen
sodom
coroner
skulls
old school thrash metal
dark angel
studs
anvil
transmetal
possessed
autopsy
whiplash
sadus
vio-lence
exumer
cryptic slaughter
excel
strike master
morbid angel
destruction
testament
celtic frost
forbidden
exciter
nuclear assault
vincent price
texas chainsaw massacre 3 sdtk
friday the 13th part 4 soundtrack
godney
baz
mick brown
extremely cheesy
twilight
spirit jazz
ecstasy and xanex
psychedelic drone
free improvisation
tropical
field recordings
ritual
ecstacy and xanex
tape music
rembetika
ukrainian alternative
virally yours
epic shreddage
improv
greatest band ever
socialist nazism grindcore
guy with beard
communism death metal
people i sodomized last night
new jersey
cover band
post-punk post punk
post-punk new wave goth alternative indie
glam new wave
the l word
dante
vergil
fena ses
samstagabend
sweet n fresh chillout
pitty
panico na tv
eu sou stefhany
ko?n
disturbed
adema
randy orton
wwe
sage francis
three days grace
sr-71
linkin park
awsome
aj
aly
crunk rock
electric
comedy rap
rouge
american singer
american idols
rbd
male vocal
aline silva
aline willy
bubblegoth
victoria justice
tea party
upbeat
hopeful
beautiful day
kerli
i kissed a girl
metro station
catchy as hell
metro
night driving
new music
good stuff
foo fighters - the pretender
paramore - crushcrushcrush
crushcrushcrush
hayley williams
superbus
the remix
anna tsuchiya
lady gaga - poker face
poker
pussycat dolls
white lies
i like this song
power song
amor
50 cent
jay-z
hick
smith
ladygaga
kanye west
dolls
pussycat
lopez
rockn
bitanemm
not like that
baby
jennifer
brtineyim
womanizer
alicia keys
dinlenir bu
mangam
ferman
cem
efe
tech house
hipster garbage
hipster
chanson francaise
favorite tracks
essential ballads
creepy
bunny
euro
happy hardcore
video game
furry
kirby
einhander
square enix
squaresoft
orchestral
madona
cirque du soleil
romolo di prisco
hurricane
yiff
luxo
depre music
bitches
ja chorei horrores
mar
kat deluna
hard core
very underrated
bluegrass
slowcore
cinematic
spacious
lost integrity
boards of canada
aphex twin
one on twoism
casino versus japan
do aliens dance in four four
ghostrock
analog
trills
hexagon sun
thought bandit
twosim
illuminant
olympia
power-indie
ghost and the drum machines
triphop
jared maple
warp
limbo
human condition
blurrypup
oldtunes
trip-hope
mankind
futuristic house
bitkins
alpha centauri rock
moon rock
mars rock
washington
vangelis
cinematic folk
fabulist
magik
gothic hip-hop
the smiths
suede
elbow
haven
bauer
interpol
cinematic rock
metallica
pantera
excellent
duran my fave from wedding album
love duran jt
jt duran andy
brillant lyrics
extreme power metal
dragonforce
melodic heavy metal
dream theater
iced earth
children of bodom
blind guardian
anthrax
kamelot
guitar gods
aina
arch enemy
new jack swing
thriller
acapella
singles i own
laid back
relaxed
britsong
gorillaz
the killers
the veronicas
night
duo
unsigned
optur
nostalgi
tyler adam
brandy
harmonies
timbaland
missy elliott
tweet
aaliyah
kiley dean
contemporary rnb
mya
nelly furtado
ginuwine
monica
kelly rowland
janet jackson
bubba sparxxx
game soundtrack
sid metal
romy
little
miss
flapper
low
nick album release pop rock dark pop
cascada
delta goodrem
mika
kate ryan
anal pop
best band of the world
anti-emo
k-on
beatbox
ukrainian pop
women
glee cast
chick music
eargasm
glee season 2
defiant
reminds me of friends
play it loud
songs on repeat
awesome lyrics
tv theme song
unnecessarily long titles
better than the whores version
too good for a fucking twilight sequel
ponto
cazuza
rappa
bosnian metal
porngrind
urlikanje
bosnian dub
southern metal
chinese
cantonese
flamenco
orchestra
string
drum
garbage
and one
techno rock
taylor
sick
poetic
acid
trashy
scene
jimmy-anarchy
legendery song
me
supreme
breaking through
crunk
cry
jj
dirty
suck me fuck me
radio killa
urban contemporary rnb
symphonic prog
neo-prog
atmospheric rock
canterbury scene
progressive black metal
prog folk
symphonic rock
zeuhl
fusionmetal
progressive electronic
psychedelic pop
psychedelic folk
melancholic rock
death doom metal
baroque pop
soundscapes
world fusion
glitch-hop
indietronic remix
franz ferdinand
the kooks
the fratellis
dirty pretty things
the pigeon detectives
kaiser chiefs
kasabian
snow patrol
the rascals
kings of leon
we are scientists
ok go
maximo park
elertronic
my chemical romance
the automatic
the cinematics
acous
air traffic
klaxons
the rakes
good shoes
one night only
reasons
rob fusari
broussard
lelia broussard
lelia
fusari
stefani gemanotta
milkboy coffee house
adam young
jazzy
under 200 listener
detroit
g unit
g-unit
west coast
adnet humor mtv marcelo
alo brother
monange
twitter
verde
the luxo ahazante tour
shitney
playback
pura uncao
puro fogo
pure fire
vixe
lixa
fuck you i just want to dance and drink tonight
sexy pop
sexy bitches do it better
folk pop
danish
this song should be single
atmospheric black metal
steampunk
ritual dark ambient
finnish black metal
occult
power electronics
kenneth anger
black noise
melodic black metal
black math blackmath chicago
hauntology
apocalyptic folk
coldwave
hynagogic
female psychobilly
fetish
neugaze
ghost drone
martial ambient
horror ambient
sound collage psychedelia electronic folk
whitemorgue
painting the end
black funeral doom
lips songs
vocally perfect
songs that make me sleepy
songs i like to sing
shit hot beat
crap
songs to make love to
makes me happy
pretentious
makes me want to dance
summer songs
mash up
awful singing
passion
good for working out to
explicit
memories
songs that tell a story
teenage angst
i saw them live
wizard rock
japanesey
euro dance
i dont give a fuck
kabriana

surrender
top ten
new releases
cyndi lauper
sandra
mara maravilha
boney m
anna nicole smith
black eyed peas
the pussycat dolls
pcd
nicole scherzinger
avril lavigne
kelly clarkson-because of you
kammylla
bep
nicole
break the ice
the pussycat dolls - stickwitu
pcd - stick with you
pussycat dolls- stickwitu
gwen
kiss kiss
wind it up
wind it up gwen stefani
pink
amy winehouse
get me bodied- beyonce
beyonce - get me bodied
beyonce - irreplaceable
clumsy
umbrella
be good to me
fergie clumsy
ring the alarm
scotland
not 4-4 time
the slow-mo
notorious
glasgow
sea
longing
resignation
bitter
separation
thunder
western-ish
football
5 star song
journeys
menace
obsession
gtasa tracks
rousing
goosebumps
water
edinburgh
big 3
storm
sheffield
the focus the fear
real hard anime music
chalga
bulgarian rock
polish progressive metal
great songs
ur so gay
et
teenage dream
cast
etherial
biosphere
sigur ros
canada
amiina
buddha-bar
the album leaf
cocteau twins
bjork
budha bar
coleen
emiliana torrini
enigma
thievery corporation
delerium
musetta
massive attack
halou
debussy
erik satie
slow dive
amethystium
enya
john cage
brian eno
atmospheric ambient classical
best cover
less than 3
wierd
cool stuff
real good
one of the best songs ever
stock aitken waterman
mama
totally rocks
thelma harper
raytown
totally bitchen
new wave happiness
tag your songs properly
artists
nz rock
new zealand
kiwi
regionalista
gauderia
gauchesca
caipira
tche
tche music
cbjr
brazilian rock
cyber metal
extreme metal
raw black metal
cold
fog
flood
plasma
washing over me
mountain tops snow melting thawing fire ice volcanos nature earth
knee slapping
bees buzzing
head spinning
determined yelling in a barren field under a gray sky
pop emo
acid hardcore
nyhc
us rock
chanson
rhythm & blues
madchester
swingabilly
hillbilly
touching
not me not i
soulful
tight
new york shit
phat
the truth
real talk
good shit
ill
the good stuff
great lyricists
solid
brooklyn
poignant
roc-a-fella
neat
dipset
real hip-hop
fiya
banger
superb
d-block
dope cut
east coast rap
ruff ryders
witty
lyricism
assertive
mean
high school
messy
kinsella
reverb
royksopp
trentemoller
trentemller
r'n'b
armand van helden
axwell
out of office
loved
skandinavian music
night music
elektropop
norge
turkish
gitar
instrumental rock
blacksea
karadeniz
ozgun
world indie
blues-rock
garage punk
contemporary classical
sounds of the universe
u2
meditation
drone ambient
must have
self made music
musical installation
forbidden sounds group
ethno
free download
russia
pagan
ronni-rock
awesome guitar jams
jimmy page
favourite songs
70s rock
tolkien
led zeppelin tracks
harmonica
cowbell
awesomeness in the form of sound
zeppelin
things i like
songs that make me sad in a good way
one time
never let you go
selena
lhp
signed
dj gk
deja vue
vince clarke
southampton
the k
lhp records
alberta
the mix kid
orlando
county
florida
us
his music describes my empty love
alone with everybody
human conditions
hot french
english romance
fuckable
and you wish you could hide
sadjes
pirry
the world guess by pirry
tom petty
the darkness litgless
joy division - the ugly beatifull
and the darkness lightless
the beatifull ugly
disorder from joy division-post-punk
word best songs
vietnam
the rolling stones
lostfreeq
new beat
chef musinum
electro industrial
central point
pirates club lounge
independent
jean marc toma
fucking selfdeep
trip beat
lestfm artist
electro trip
psychadelic
skinny puppy
electro trance
low tempo
dark music
poetic rock
post-jazz
pigface
-pearl fashion music
techno trance
ultimate favorite
mike patton
al jourgensen
funk metal
chaotic metalcore
weird
sound collage
pure sex
bds
ivete sangalo
ciara
hugo pena  gabriel
chicas picantes
happy metal
metal melodico
female-vocalists
death hardcore
via
daddy
cutest thing
lets move
just cool
brat2
really sad thing
bono
london
incomparable
finnish metal
finland
symphonic power metal
old school black metal
symphonic
nordic metal
true norwegian black metal
norwegian metal
music for my soul
braindance
pozitive
brain massaging device
soft music
notre dame de paris
to com for my brain
mixture rock
nitre dame de paris
noise-rock
electro post rock
musicle
hardcore trance
psy
maybe post rock
xtc
favorite lyrics
zajebiste
super
franek
franek kimono
diffrent
super extra
awsome lirycs
the the
wilco
northampton
gomez
the national
david bowie
cracker
the replacements
badly drawn boy
calexico
jim morrison
camper van beethoven
califone
the doors
paul westerberg
mystics anonymous
andrew bird
nick cave
boredoms
ooioo
iron horse
matt pond pa
wye oak
score
haunt
ted leo
the hold steady
the shins
archers of loaf
american football
john lurie
band of horses
the new pornographers
chuck norris approves
johnny depp
alan rickman
peace
nemesea
nemesea lost inside
chanteur de cris
punk musette
gospel brasil
anime soundtrack
epic horns
real blues
pissed off
novelty
paigecore
female pop singer
piano songs
funny lyrics
simlish
the sims 2: pets
twins
the sisterhood of traveling pants
piano song
pop 90
fave songs ever
indie british
american trad rock
plagiadora
punk horror
fighter
dirrty
ndw
humppa
megahumppa
excessive12inch
omd
guitar greats
early league
midge ure
rock-ballad
pedobear does approve
blue
splin
rockapops
jesus
android
bands
crow
philosophy
mood
tender
brazilian bands
sinewave
psicodelico
loca
jackson 5
avril
everlife
veronicas
jojo
jonas brothers
aly aj
goddesses
straight up modern rock
weirdness
feelgood
sex rock
beautifulsad
britrock
falsetto
live recording
ritual music
southern fried rock
spoken word
prophets
strings
dance beat rock
epic rock
mellow rock
quiet
creep
diante do trono
megadeth
kerry
'80s
wyjebany w kosmos thrash
nsbm
satan
mustaine
metallica death magnetic
horde
deep purple
acdc
mexico city
89
san diego
musical orgasm
30s
20s
dm
hutch
kurt
amazing lyrics
songs i absolutely love
sunday morning music
beautiful voice
all-time favourite albums
aly and aj
singer-songwriters
sisters
78violet
lindsay
lindsay lohan teen pop
rock chileno
opera rock
carey
tags
as
a voz mais bela de todas
esmaga as outras cantoras atuais
a melhor de todas
grande cantor
eterno rei
melhor da alemanha
legal
grandes
melhor banda feminina
muito boa
strange in moscow
melhor musica
melhor dos anos 80
exid records
spontanious
hardbass
disturb
multigen
av-hood
ultimate vampire song
sexy yes yes yes
best male singer ever
better than the a-side
reasons to get up in the morning
aphrodite
goddess of pop
melhor banda do mundo
hebrew
marry me
i can imagine that song in a movie
makes me feel sexy
i want to be independent listening to
malaria anthem
rainha
ringleader
hayzay
awesomesauce
pure win
xp8 ebm futurepop
futureundergroundcyberkore
bandas venezolanas
rocksteady
psicodelia
lovers rock
two tone
skinhead reggae
ska rocksteady
british reggae
venezuela
early reggae
j-ska
ska-jazz
rub-a-dub
afrobeat
ethiopian groove
makossa
mbalax
vudu
zulu
frobeat
juju-music
amazin voice soul to hot
psychedelic pop electronica
rap metal
desert rock
shoegazing
proto punk
really drunk
crabcore
specially addictive soundtrack
hamburger schule
legendary
pop legend
the next princess of pop
mactonight fav
dancey
poppy
mactonight essential
spacey
midtempo
amazing live
essential
cuckoo
oriental
beat
lol
prune
long island
compressive rock
the funeral drums
saw
apologetic
blue eyed soul
tripped out
uses the term gay as an insult unrelated to homosexuality and through this causes a significant amount of confusion
dark cabaret
deep-house
balearic
wonky
nu-disco
laptop folk
home listening
indie dance
narcotic house
slow disco
western-africa psychedelia
nigerian funk
dub-house
future soul
dub-techno
dial
hyperdub
bassline
tech-house
eletronic
ryca
poderosa
poder
xuxa
luxuosa
rapido ricardo
diva sambista
heavymetal
syphonic metal
fucking great
dave is dancing
dave gahan is showing striptease
datura
electro rock
best album ever
guitar hero 3
painfull
sob
violator
magical
remastered
space age drama
stoke-on-trent
stoke
keele
jacobins
the jacobins
keele university
lp
slamming brutal death metal
canadian rock
guns n' roses
a day to remember
strike
lolene
the electrick hotel
neon
rich
lionheart
hard-heavy rock
hard prog
heavy prog
progressive rock related
pop/rock
italian progressive rock
italian symphonic prog
neo prog
folk prog
aor
early heavy metal
brazilian music
manguebeat
timbaland knock
somebody to love
dels
sarah brightman
heavy nopal
question of honor
luna
astrrid hadad
in the kingdom of darkness
wonderful world
norah jones
harem
its a beutiful song
this love
rainy days
carole king
xmas
voices of angels
levitation
talented ladies
galvao
alternativerock
popbritish
iconpop
pop/r&b
motown
popbrasil
lovesongs
classicmpb
funk carioca
we belong to the music
cyrus
lindsay lohan
paris hilton
satellite
inxs
billy corgan
smashing pumpkins
folk music
devotional
tatarcha
nine inch nais
nin
kmfdm
bronx
spandau ballet
depeche mode related
4ad
johnny
cocteau twins related
lottie
personal faves
siouxsie-solo
fashion related
lenda
legendaria
princessa do pop
full on
dance-punk
spanish indie pop
dominican
latin rock
very kvlt
killer drums
killer riffing
norway
mexican black metal
norsk arysk metal
kvlt
greek metal
epic black metal

mexican metal
usbm
folk black metal
death thrash metal
unglaublich
andy bown
alan lancaster
john coghlan
francis rossi
rick parfitt
rhino edwards
jeff rich
matt letley
australia
old
bill
comets
haley
serge gainsbourg
jane birkin
psychadelic rock
gilmour
roger
waters
mason
rick
wright
david
syd
barrett
voice
paris
7th floor
foo fighters
van halen
kiss
steve vai
dickinson
british rock
drum'n'bass
black hoe recordings
mystification
albums i want to own
rhythm
bittersweet
jangly
badass
armenian language
one hit wonder
september
folk-pop
armenian
armenian folk
ethno-ballad
chant
gregorian
concert
beauty
director
queen forever
the best show
the best women singer
the richest singer
bigdrums
alt. rock
fear
bowie
makes me happy as fuck
earthling
god is an american
static-x
industrial nu metal
agresive metal
brit music
atmospheric rnb
dixie chicks
anti-bush
texas
anti-war
banjo
fuck bush
i miss you
songs i relate to
grammy
she kicks ass
heartache
heart breaking
december
makes me want to cry
queen of soul
tim mcgraw and gwyneth paltrow
miranda lambert
country women
rolling stone 500 greatest songs of all time
i love singing this
sad song
peace songs
girls kick ass
barrelhouse covers
tunes for a brother
voodoo women
delta blues
gospel of shawn
chicago blues
blues fusion
memphis blues
new orleans funk
new orleans
texas blues
neo-gospel
revolutionary
jazz guitar
outlaws and gypsys
bebop
cowboys
neo blues
southside of chicago
a brother and a friend
curious about
mad about
my teens
never enough
hindi
let's dance
work of art
heaven creation
strong
electrefing
divine
awful
very dreamy
medley
divo
rock latin
lloyd banks
cashis
sexy songs
officially shit
better than hard candy

cine
avril lavgne
restart
hc
emotional hardcore
etno rock
duduk
beautiful lyrics
fave female singers
strong voice
repeat
revenge
laxative
death-thrash metal
death trash grindcore metal
groove death metal
death thrash grindcore metal
baumarkt
moelln
death-trash metal
toiletten stoner metal
toilette
death-thrash grindcore metal
friends
death-thrash
follow me
fuckin great thrash
eath-thrash metal
fuckin brutal
death thrash
fuckin metal
kill metal
grind death
death-thrashl grindcore metal
extreme brutal death metal
surf rock
light

kids
celine dion
bubble-pop
pioseneczki
worldmusic
european music
check out
puppy et belle
solo artists
melodycore
kurt cobain
anarco punk
favorite bands
kooks
lithuanian metal
patriotic  metal
patriotic metal
lithuanian
brutal beer noise
brutal beer metal
pagan black metal
lithuanian black metal
flar flar flar
flop
fat
anahi
latin singer
brega
uuuuuuuuuuuuuaaaaau
bjmeligafui
ate my heart
guitar playing boys
my pop music
songs to monkey it up
21st century girl
qq
hm
mraz
fuck
axel
national anthem
brothers
nick jonas
criollo
frogadelic rock
viking quest
we love the 80s
dark and new wave
alevi deyisleri
persian
political
fok
br00talizer shits on phoenix azul
pile of feces
shit in my ass
shitting spam
juggalo
jizzy
capitalist
br00tal
ofwgkta
really good
radical
anarchy punk
charmed
pop sensation
rock metal
pop female
opera metal
celtic rock
netherlands
scrubs
jazz-rock
guettotech
classic prog
industrial groove metal
dominican heavy metal
kietas radijas
lithuanian hip-hop
lithuanian hip hop
mc numeris vienas
kaunas
doo wop
classic country
am gold
prism
lynyrd skynyrd
home lounge
indie/rock
big hit
punk/rock
bitchy
scandinavia
oblique
barcelona
sonja
without making noise
90210
ugly betty
xenomania
m.i.a
van morrison
joshua radin
josh rouse
credence clearwater revival
scott matthews
beach down festival
jose gonzalez
ray lamontagne
indie-folk
damien rice
aimee mann
alex newport
frank turner
a7x
avenged sevenfold
slow rock
amerie
indy hip-hop
indie rap
anticaptlismo
libertarian rap
anarchy rap
ldl
anticapitalismo
russian underground rap
japanese grindcore
female fronted grindcore
ptu thrash metal
satan brutally explodes when listening to this
antifascist
antisystem
antifascist rap
audiobooks
stalin fm
alternative rap
melodic antifa-88
poebotina kakaya-to
neudachnaya parodiya na zapiski neizvestnogo
vegan
hardline
never goes away
relation or what
associative
beautiful voices
female fronted
true blood
waking up
ready set go bitches
club banger
gangsta
hottest bitch
steven wilson
progressive punk
tool
my favourite
francais
camp
apartheid
boppy
steve biko
south africa
rap francais
choir
choral
electric guitar
cover better than original
tomoyasu hotei
long track
miami vice
kill bill ost
english
homophobia
coronation
handel
czechoslovakia
my favourites
post-punk revival
alternative hip-hop
post-britpop
electro-rock
solum mente infirmis
ecological power metal
eco power metal
pure rave
ambiental blues
if this was a pokemon i would catch it
ambiental
highschool band
romanian band
ceara
melhorbandanacional
metaaaaaaaaaaaaaaaaaaaaaaaaaaaaal
louvor
worship
worst song ever from miley
valentines
marry taylor
swifty
500 days of summer
thom yorke
jonny greenwood
french vocalists
garou
godfather of moonwalk
genius of pop
legend of music
fantastic artist
symbolika muzyki
muzyka filmowa
beat from dominicana
kal ho naa ho
favorite celine songs
french rock
french bands
power rock
rock français
rock alternatif
autoproduit
toulouse
destroy
planant
creative commons
jamendo
french punk
punkrock
whawha
festif
puts every single hater jealous
songwritter
actress
the best thing that ever happened to music
comeback
rock n' roll
idols
finnish pop
excepcional registro
tecnicamente impecable
buena instrumentacion
los mejores
un icono
otro genio
blues influence
bueno realmente
un clasico
un buen tema
crooner
ballet
all dressed in black
the raves
shoegazer
violinist
sune rose wagner
alaska
david sylvian
nicky rhodes
porcupine tree
rock argentino
politeconomist realife soundtrack
classical piano
mina love

beautiful man voice
pop latino
algerian
summer time
guetta
mazzy star
yo la tengo
suicide
throbbing gristle
stereolab
squarepusher
kings of convenience
sonic youth
cabaret voltaire
spacemen 3
the slits
wire
spiritualized
doves
broadcast
pixies
dead can dance
autechre
mercury rev
tortoise
tuxedomoon
iggy pop
blonde redhead
fridge
air
the fall
lamb
kraftwerk
fischerspooner
nine inch nails
acid house kings
throwing muses
my bloody valentine
nouvelle vague
portishead
cranes
mogwai
this mortal coil
fall out boy
taking back sunday
boys like girls
brand new
fl
spaccaculi
christian alvestam
russian melodic death metal
russian metal
emo rock
awesome track
atb
pompa xd
mbrother
polish music
loneliness after dusk
ghost in the shell
japanese music
awogahd
boston
gothic pop
nina
aretha franklin
nina tolins
ninasings
ledzepplin
tina turner
maroon 5
elisa hale
magicsong
style
sing
rocketeriarecords
ashley simpson
liarliar
pat benitar
rebel with a cause
ninatolins
jonny lang
sexy baby
patbenitar
therunaways
etta james
newalbum
jazzmine sullivan
gwenstefanie
rocketeria records
random songs
prince
hubert kah
my darling
pokemon
old school kircore
arcady
alcohol
ed banger
hooooooooot
powerful voice
show woman
gritos ressussitadores de mortos
my favorite albums
basic pop
porcaria viciante
best ever
insane tracks
great dancers
great bands
great colaboration
quality pop
too good for american idol
exttravasa
great singers
quero apertar esses peitos
best dj
best dancer ever
it could be so much better
if this were a pokemon i would catch it
chiclete
eurovision song contest
polskie
polish soul
esc
saz
oud
turkish folk
jewish
tamamen ozgun
middle east
middle eastern elements
kurdish
kurdish folk
female voice
best composer
afro-beat
progressive middle east folk
oriental jazz
iran
kora
farsi
turkish influence
nazim hikmet poems
zeki colas
smallville
buffy
privileged
couples
chuck
friday night lights
roswell
veronica mars
the oc
criminal minds
vampire diaries
kotoko lament
day after tomorrow - stay in my heart
zard - hero
yui - life
false grind
doom
evil
doomviolence
cardboard city
cairns
sotu
iamx
hourglass
oh well
middle eastern
percussion
saidi
traditional egyptian folk
disneychannel
tus ojos
passado
tal vez
kudai
nx zero pop rock novo
pop rock cover pop dance identidade
gta iv
musica brasileira
stronda music
stronda
baile funk
sertaneja
gabriela
ferinhas
bds no coracao papitho
feel-good
yiruma
steve barakatt
dishwalla
life in the city
j-cuties
asian mash ups
mash ups
neo bitch in training
featurings
-the mind trip
infinity b-day megamix
the power rangers of pop
queer songs
club mixes
paradoxal songs
addicts
pure fucking metal
progressive thrash metal
technical brutal death metal
pathogrind
russian folk metal
groovy death metal
gaycore
dbz
finnish folk metal
experimental black metal
atmospheric doom
blackened thrash
medieval black metal
folk viking metal
creed
guns n'roses
legião urbana
renato russo
indian
my fav songs
skins
best serie ever
female gothic
lyrical
pop bizarre
acousmatic
jam band
david guetta
david archuleta
kris allen
colbie caillat
manoela gavassi
capricho
new romantics
excellent synth work
orgasmic
i want back to the 80s
pure genius
steven strange
tik and tok
gods
minimal electro
blitz
duran
futurist
gary numan
tiktok
real man
la richards
my favorit music
nick beggs
japan and ultravox
fairytale
serbian
alternative country
hardcore rap
classic rap
west coast rap
female electronic
classical terror
tribute
singer songwriter
mute
curve related
crewe
coverband
tony soprano
martin gore
wanted soundtrack
b-side
streamable album
amusing
shot risk
mcr
cher
n 1 hot 100 billboard
sticky and sweet tour
bucetacore
laura pausini
waaaaaaaannnnnttttttt
new comers
addicted
simple
the voice
awesome ballads
godtina
she owns the throne
crime
songs i cry like a bitch to
not placebo-like but still good
weird but good
awesomeee

approved by goku
nineties
approved by prince vegeta

under two minutes

reverie in me
oceanside
bunnymen heaven
ant farm
electro-acoustic
jimmy pages firm
full-on
top 20
spacy downtempo
trailer music
souls
native
opera-like
like
check your meta tag
oui fm
80s metal
at work
1008
guerilla
more plays
queen of last fm
love gaga
little monster
queen of dance
just dance
the best singer
fame
piece of brit
rock me in
rich and famous
let me be
lover
brit
cherish
cassie
brooke hogan
mary j blige
paula deanda
loud
godga
cannibal
blow
shady
marshall mathers
mathers
yelawolf
em
yellerr
detroit rap
shady records
recovery
d12
aftermath
weezy
drizzy
interscope
movie score composers
slipknot
melodic death doom metal
symphonic folk doom metal

the corrs
celtic tribal
evanescence
neon trees
rock-a-rolla cover stars
rock-a-rolla magazine cover stars
funny shit
modern talking
shit glam
rocker chick
rockville ca
playlist
alex patsavas
lissy trullie
lykke li
the asteroids galaxy tour
say hi
coconut records
passion pit
the temper trap
the republic tigers
the little ones
janelle monae
blind pilot
malbec
elvis perkins
oppenheimer
noisettes
grizzly bear
anya marina
gotye
the duke spirit
nico stai
my number one
dark wave
lustpop
dd covers
old school ebm
darkwave-synth
wave
electro-goth
bastardpop
sisters of mercry clone
performance
dd solo
minimal electronic
melodic power metal
hard pop
resident evil
rominhocsf
testana
atari electronic
microhouse
romanian folk
romanian
lets dance
a cappella
true metal
beatles
tony hawk underground 2
tekkonkinkreet
a town in blue
nsync
simple plan
just a girl
song  of the decade
jordin sparks
shakira - give it up to me
freshlyground
the best official world cup song ever
apologetics
debate
christianity
gorgeousity
nasheed
filmgroove
audiobook
impossible for liberals to deal with
faith
bible study
otaku
science
conservative
chanting
cult favorite
talk radio
doctor who
richard dawkins
small government
bellydance
porn soundtrack
islam
religion
spaghetti western
atheism
own it
liberty
ummah
porn music
liberal media
kazu
boss sound
jamaica
pioneers
traditional skinhead
dirty reggae
trojan
skinhead
pama
rude bwoy
oi
psychobilly swing
neo-swing
traditional ska
good 3wave ska
2tone
jamaican ska
oibilly
giddy-up giddy-up
symarip
toaster
neo rockabilly
irish punk
irish folk
indonesian
the aggrolites
heavy organ
folk punk
peter tosh
collie
skinhead raggae
i am home
makes me bad
hard on for jesus
sweet to taste it
road trip
driving me insane
sleazy
driving
amazing grace
i dig you
gospel-noise
sexy and energetic
weird drum solo
best morning and midnight music
cowboys and angels
bad old times
rock anthem
best song on album
bernard butler
girls night out
brawling
one of the best songs of 90s
midnight where the day begins
oh ohio
my university
good old days of champagne
rock steady
paranoic
on drugs
melodramatic synths
riders on the storm

one of the sexiest songs ever
sexy as hell
dream-folk
best advert music ever
sad and beautiful
heavenly sweet
dudelsackrock
darkwave gothic
dad
gothrock
asp
sisters of mercy
in my heart
unheilig
gothrock gotik romantic sad
temple of the torn
mick jagger
moody
southern
melodic hard rock
guitar riffage
top 24
tim urban
top 50
luke shaffer
ska metal
geek
nerdcore
german hiphop
berlin
hamburg
stuttgart
austrian
hessisch
choon
incalus
vikernes inspired happy frostbitten black viking thrash pirate metal from the fiery pits of serious business
maltese
bosnian
paradiso girls
bajan
barbados
slovene
belgium
moroccan
malta
calypso
bahama
caribbean
guys i would fuck
please let me lick you
korea
balkan
untouched
chicos
dantes
fabrika
popular music
masha fokina
para normalnyh
2 normal
anti-crisi girl
svetlana lboda
loboda
svetlana
nu virgos
be my valentine
ukraine
mogilevskaya
real o
ukrainian music
future fusion metal
shoulhavemoreplays
soul sexy
hannah montana
gloom
moby dick
chilean rock
fucking fuck
prog math rock
death rap
good songs

top rock songs
all time favourites
2-step

sexxatraxxion
latin boogaloo
favorite album
chamber pop
afrikaans
dancer
destiny
attention seeking whore
sucks
glitter
glitter bitch
another reason to hate canadian music
mini-rihanna
devil
superman
best rapper
lovable
most hated
belly dancer
shaki
most talented
best dancer alive
gone too soon
gone but not forgotten
fatty
the vampire diaries
polish artist
sentimental
post screamo
polish music drama
best prodigy remix
easy a soundtrack
most beautiful
words are cheap to describe my love
i listened to it over and over again
to sing
so beautiful i could seriously cry
a piece of me
happy happy joy joy
songs that make you want to sing along
amazing singer
eargasmic
brings tears to my eyes
song for rides
optimistic
creepy but amazing
favourite lyrics
when you are happy or unhappy
can't stop listening
cheers me up everytime
best cover ever
songs i am likely to start singing loudly in public
no escape from loving it
my mind is literally blown to pieces
countrycore
anthemic
wistful
stylish
red red groovy
majestic
97x
james bond
village
lyrics
siouxsie
bounce
lag
stones throw
anti-folk
gay fish
electronic indie
guitar-based digi-bongo acapella-rap-funk-comedy folk duo
melodic rock
life space
mind-bursting
bright
love itself
saving grace
impartial
higher force
credo
arrogant
unconcerned
pop collaboration
rick ghraphixxx
east-coast
rick-villa
ft-lauderdale
rick villa
lupe fiasco
south rap
street vanacular
drake-like
emir
no1fan
hiphop classic
808 and heartbreak
east coast
nas
relaxed-music
apex hills
apex-hills
like rick ghraphixxx
fond memories
808s and heartbreak
rick villy villa
sickness
g-o-a-t
dope rhymes
bob marley
lovin it
lamangous hiphop
streetrhymes
southern music
gezellig
allgemein
aces
adds
psycho stoner
psycho stoner rock
80s heavy metal
80s hard rock
stoner psychedelic dull rock
southern stoner rock
stoner
stoner power rock
mosh metal
rik emmet rock
just enjoy
mountain music
for goodnight
deutschrap
dancecore
trivium
bloodhound gang
norma jean
russian shit
soviet
post-industrial
synth-goth
shelter
memiors of an imperfect angel
tricky stewart
leaked
brandy norwood
long distance
human
list
crazy amazing
nerina pallot
deutsche schlager
synthesizer
ural
lightwave
urbanwave
cigarettes
robots
co
heretics
sailing
español
cities in dust
better than britney spears
better than madonna
rainha do rock
hino nacional brasileiro
club trance

eclipse con las chiicas 8-7-10
floraaah 3
vos patiito liiindo :3
nick y haylie
sam and nick au
esos momentos de horarios raros para ver erreway en el 9 comentando con siss nuestras parejas cambiadas jaja
con voos en el autoo viejos tiempor tro tro
adiction :d  lll
jaime siempre con argentina -l-
papii robert baba lll
cancion para llorar un dia depre-amo este tema :d l
divina y yo -- 14- 01- 09 --  17:45  --- sala 2  -------- twilight    lll
el mejor regalo de ella con esta cancion 3
el mejor regalode ella con esta cancion 3
nick and haylie
noches de insomnio mirando video lll
eclipse   lll
coreo  full :p
tiempos tristez y raros en floricienta :c
ayy divina te acordas en la casa de la tia con el grabador cantando
ayy que epocas viviamos suspirando por el freezer lll :d jaja
t-kiero florr
recuerdoss de la infancia con siss
viejasoss xp por dioos el extrano de pelo largo sin precupaciones va :p  3
viejos tiempos lll
er
cd de britney en el auto con replay para cantarla con florr
love ya  lll
supergroup
space age pop
classic sixties
neomelodico
mito
oldschool hardcore
haotic
great heavy metal band
the voice of an angel will echo in our hearts forever
the greatest hard rock band of 60s
great hard rock band of 80s
they will rock you
the best rock band ive ever heard
a well-know classic rock of 70s
really nothing else matters they are my favourite band
they have made the story of the rock n roll
great unforgettable rock band
the greatest hard rock band on earth
satanic black metal
post-avant blackened shoegrind
messenger of the apocalypse
portugues
boyband flopada
bando de gaydacu
backstreet boys da favela
born this way
thrashdeath
arniecore
pagan industrial progressive gay pop
mtv black metal
fag
swiss
oldschool death metal
drum machine
galaxie 500
sound effects
aberdeen
sfx
james watt
baixar
fucking epic
fucking great cover
matell
diana ross
crystal gayle
top 100 pop songs
forever young
bob dylan
tinband
thomas ian nicholas band
the way we were
barbara streisand
leon russell
elton john
country top songs
top 100 artist
country songs
top 100 single
whats goin on
marvin gaye
strange fruit
billie holiday
jake gylenhaal
heath ledger
brokeback mountain
ambiant
classique
shania twain
faith hill
nickelback
zac efron
cast of high school musical
high school musical
down tempo
don giovanni
tim burton
seiyuu
j-punk
holy shit this music is better than sex i think im going to go murder my dog this is so awesome
j-hiphop
okinawa
para para
monster
sexy boy
latin singers
fuck me
puerto rico
sergey
gagalicious
glamophonic electronic disco baby
merengue
111
gul erda
murat kekilli
spanish metal
teocida
estudios teocida
zaragoza
aragon
beck
the avalanches
smoke city 33333
shanuss
night and day
vaughn
ellllla
pulp
music to fall asleep to
good finds
bent
amazing jhonny cash
grand central
bootie bompin
oh damien
ever
stefani
new yorkerin
germanotta
sterben
die
lovesong
the
schlecht
angelina joanne
stefanai germanotta
chillil
i like it rough
jazzpop
light music
los angeles
psych
gene clark
emitt rhodes
the small faces
humble pie
immediate records
feel good
free
amanda palmer
great bassline
great riffs
long
tristan allen
afp
illadelph
dungeon family
rhymesayers
mixtape
consience
montreal
halifax
freight train
mistah rapsey
talib kweli
nigerian
african
africa
army of pharoahs
sour diesel
jedi mind tricks
slum village
def jux
horror metal
pirate songs for piratey pirates
punk n roll
bands that are doing the same ol thing but it still sounds good
2p falls theme music
the vickers
firenze
italian alternative
keep clear
cool music
irish singer
elettro pop
elephant six
elephant 6
hippie
rio de janeiro
neo-hippie
potiguar
psicodeliabr
ramones
viaovivo
animals of many kinds
lsd
athens
ska-reggae
nonsense
ex-ska
godness
super singer
myself
shitty sludge
killing emokids
much better than metallica
protopunk
american skool of punk
amazing british punk
glam punk
punk'n'roll
sleaze metal
nworsm
watched live
the new wave of classic rock
beatles julia
rebetica
from the botton of my broken heart
french reggae
great play
gentle giant
rush
pop pank
ska-punk
skate-punk
emo-punk
melodic hardcore punk
industrial punk
ragga punk metal
so fucking cool song
motivating
latin style
most romantic songs ever
titanic
baby jane
high notes
honesty
mmc
jordan bratman
better than the original
intro
the queen of dance pop
the queen of pop
morr music
carte postale records
city centre offices
n5md
tripostal
constellation records
leaf
eglantine records
minimalism
temporary residence limited
type records
domino
tabla
asian underground
electro-dub
game ost
industrial jungle pussy punk
costa rica
ruben blades
nada
pearl jam
cantautor
balada
edie brickell
really good music
rsonists look alike
based music
avant garde hip-hop
glitch glitch hop
white american
shibuya-kei
jazz hop
pedocore
brings back memories
driving songs
exactly
skacore
alternative-indie
indi
folk matal
bass virtusos
the best bassint in the world
cliff is and will be the big cliff
god of metal bass
jazz metal
techinal death metal
freak
folk-drone
4m4zinq
f4nt4stic
bri11i4nt
motherfuckin rabbits ejaculating sunshine
power stance
ear rape
new zealands fourth most popular digi-folk parody duo
what is this
i pretty much cried when you broke up
i brush my teeth to this shit
bitches weaves hoes leather jackets
the most amazing song ever written and i do mean ever
i heeeeaaaaarrrrrr vooooooiiiiiccccccceeeeeeeessssssss
always i wanna be with you and make believe with you
hard rock revival
deathpunk
designer
designer rock
yagaced
music for sunny days
favourite worst nightmare
porn folk
nerd rock
garage rock revival
uruguayan
high energy rock n roll
nokia 7500
theremin
almond
the best music
makes me cry
great vocals
good voice
excellent vocals
fuck-track
polydor sucks
she knows you out there
party starter
the drunk one
fashionista
the cute one
so damn aorable
lets break it down
grammy nominated
belarusian
bobruisk
leva
shura
patriotic
belarussian rock
belarussian
belarusian rock
rocr
william orbit
best remix
beastie boys
music to listen to at night
sexiest man in the world

madonna demo
rammstein
the prodigy
alan wilder
stand-up
wth
pop bitches
rock pop
iamamiwhoami
fairies
estonian
best song on the album
best of me
ghotic
motley crue
rock brasileiro
rammstein - sonne
tiesto - cary brothers - ride
tiesto
bjorn
jesper
peter
daniel
in flames
anders
alexi laiho
solo mastery
chuck schuldiner
metal legend
dominican metal
guitars
rock dominicano
made in the dr
made in dominican republic
dominican rock
must hear before you die
great stuff
melodies
synesthesia
casa de teatro
trova
opeth
desperate housewives
rat food radio
somber
plotting
beatles cover
ramonesy
claus grabke
headphone music
sleazerock
where is my bong
my favs
ramonescore
darby crash
skate rock
the kinks
dada
80s hardcore
prog kraut rock experimental
art punk
talk n roll
kbd
77 punk
dogtown
skate music
thrash speed metal
teutonic thrash metal
bay area thrash metal
first wave of black metal
strong vocalist
merda
sampler
bossa rock
circuit bending
stewart
koda kumi
boa
post metal
post grunge
male choir
boy group
cover songs
boy band
television
wilmie
wilmiepieroni
pieroni
justin bieber
pretty reckless
pixie lott
pop singer
boomkat
jennifer lopez
ashlee simpson
violao
rock brasil
crista americana
alternatvo
extreme progressive metal
extreme majestic technical epic melodic metal
french death metal
progressive folk rock
best albums ever made
united states
music to slay dragons to
symphonic techno
soundtrack techno
goth metal
surpising
godlike rhythm
amorous
already the best song of the fucking century
scary
i love freddie mercury
iphone
chip music
beep
beep beep
strange
bizarre
bizzare
nanostudio
bow down
dave gahan is a sex idol
polish jazz
20th century classical
people who are freakier and folkier than motherfucking devendra banhart
tru black satan pr0 norwegian northren uber brutal grindcore progressiv vegetarian technical violent porn metal from kazahstan
obrazoburcze
xcause
cagada
fusion metal
rock kabyle
gnawi
pop life
pop pleasure
fantastic pop album
pantanal
90 goods
power voice
trilhas
novelas
90s pop
girl shit
favoritas
popgroup

female artists

chilled
electronica roots
shemakesmefeellikeshit
good screamo
hawt
great :p
electronic pop
sex education
electronic cover
stutt-ing-stutter-in-stutter-en-ing
coolemo
electro dance
emotronic
power pop punk
superpowerpop
hip hop screamo electronic
optimistic electronica
loopiloopilooploop
thbstofthehistory
altrock
hammers and hearts
so hot
traga la bala
utrainedtheselipswhentheywerechamps
gabegonnagetuhigh
weee
fuckmeinthebackseat
pants on fire
musulman
cuzifujumpiwilljump2
salsa pop
hip hopcore
japanese rock
chinese pop
korean pop
japanese trance
cantonese pop
korean hip hop
tense
sophisticated
soothing
even better live
korean trance
korean rock
oshare kei
korean ballad
best live album
harry potter
i love
addiction
eletropop
promiscuos
relaxxx
tchubaruba
ghettotech
miami
mad
f-e-r-g-i-e
hips dont lie
fun rock
garage band
obsessive songs
brazilian funk
dance all night long
calleiente
obsessed
beauty killer
party party party
perverted
sluty
fucking love this
gonna marry his voice
hooker
phoda
euri
best best best
foloka
queen of the internet
awesome covers
i love it
muuuito boa
bc13  motherfucker
party music
a party in your bedroom all night long
rock blues
stevie ray vaughan
creedence cleawater revival
gothic-electro
industrial metal gothic
panic at the disco
good charlotte
confessions on a dance floor
remix - paul oakenfold
dance queen
ancing queen
gaucha
uruguay
nativos del rock
coutrycore
post core
boysband
acustic
singe-songwriter
disne
brasi
girlband
neo-thrash
hip hop/rap
wu-fam
uk hip hop
wu-tang
boot camp clik
uk punk
pioneer
speedrock
descoperiri
aana
special
jon b
brian mcknight
keyshia cola
oops
kelis
jill scott
boyz 2 men
pharrell - our father
joe
bed
letoya
fuji bana u
hi five
conya doss
give in to me
cash money
shawn desman
keyshia cole
212
john legend
45
ms dynamite
from u 2 me
chris brown- winner
michael jackson and kanye west
112
not lookin
marques houston
1
all saints
je
r kelly
toni braxton
jay-z -wishing on a  star
tlc unpretty
you gotta be
tlc
robi draco rosa
draco rosa
draco
rhythmic noise
knarziges klickgefrickel
schranz
cryptofa
netlabel
ruckno
didgeridoo
mash-up
brett
manipulated monkey music
metall
mathmetal
punk folk
the silent force
manu chao
within temptation
gnawa
gnawa diffusion
gnawa tradition
ghouti
dorme
beeshop
felipe
inverno
smu
dilema
pornogrind
txdm
slamming guttural brutal death metal
blue deep
r kenga
rock independente
rock in roll
stoner rock brasil
rock in rock
fasten seat belts
direito de ser nada
violins
british pop
brazil hardcore
animals
slow build
neoclassic
wonderful through headphones
headphones
psy chill
hiptronica
psymbient
techno-rock
christian music
awesome god
amy lee
hillsong united
bandas nacionales

alabanza
adoracion
musica cristiana
icthus
next big thing
bitchy lyrics
happy song
american idol 9
female duo
weird lyrics
lyrics without any sense
stupid lyrics
girly pop
manhattan socialite
not the best singer
french indie
glam pop
love flowes on the grave
banda eva
lindas
darin
lindas e poderosas
studying
life
high
sleeping
sleep
lsu
pump up
new orleans jazz fest
neil young
sexsongs
music to get high to
my favorite
best indie ever
funky beats
us metal
great intro
great riff
guitar god
adrian smith is god
great solo
italian metal
canadian metal
super bass
american metal
uk metal
great b-sides
best of the beast
traditional metal
belgian metal
us power metal
chipcore
picopico
vocaloid
shranz
hardbase
hardtek ebm synthpop
chitune
vocoder
thieves
hard house
montreal local
lesser known yet streamable
montreal electro
lily allen
turkic
central asian
turk
kyrgyz
central asia
kyrgyz pop
kyrgyzstan
kazakh
kazakhstan
kazakh pop
uzbek
world pop
uzbekistan
uzbek pop
turkic world
native american
turkish pop
kazakistan
etno-pop
flute
native american flute
ethnic pop
turkce
etno
turk dunyasi
mocne
song that makes me cry
i hate you
cold wave
go go go go go go go go gadget flow
tron legacy ost
expeirence group
hip-hop and pop
happy happy birthday to me
impressionism
old england
dark prog
acid folk
traditional irish
japanese folk
arabic metal
flauta
classical guitar
sunshine pop
bonus track
arrival

waterloo
voulez vous
the visitors
abba - album

the album
gold - greatest hits
18 hits
spanish version
abba - live
tour 79
the girl with the golden hair
wembley
ring ring
in concert
german version
french version
darvin
mcfly
lifehouse
diana agron
quinn fabray
alice in wonderland
quinn
jessica sutta
quinns2
grey's anatomy
skins season 3
misfits opening
misfits
oth
hard-core melodico
screamocore
feel good music
experimental night pop
dream wave
experimental pop
cafe music
epic cover
spoken words
sloveniain
nu disco
you me and a field
indie jazz
way too fucking lo- fi
my boobs can tell when its gonna rain
colombian
music for making pancakes
rock gospel
coisa de deus
post modern
listen with headphones
21st century
art music
contemporary chamber music
handclaps
wall of sound
daft punk
classic soul
rhcp
love ballad
best of rem
superlove
favourte tracks ever
coolest guys
what a masterpiece
my rhcp favourite song
post-black metal
medieval metal
drone metal
experi
psydub
goede minimal
zen
ikebana
skins soundtrack
complete sentences
colorado
fashionably punctuated band
the fact that this is the name of a band makes me want to check their music out even though i do not care much for indie pop
extraordinary band names
just some guy
one of my favorite songs that i always forget about
kevin devine
denver
makes me trigger happy with the love button
colorado springs
jesse lacey
modest mouse
greys anatomy
sin city
scenester rap
as if a creepy bug was singing beautiful songs to you on your daily walk
music to chase squirrels by
play just some guy radio just some guy
daytrotter
day
songs of love and madness
songs that mention other bands or artists
skins ost
flcl
dreamhouse
slutcore
power ballads
easy
new brazilian artist
krutt
faroese
classical rock
pure rock sound
persian rock
rac
american rock
tag
real screamo
sludge doom
real emo
metallic hardcore
deathwish
skram
nyc
ewoks
beard
ukhc
nublado
prr
modern hardcore
modern screamo
atmospheric sludge
berlin school
hawaiian
remixed
polinesia
mongolian
folkloric
multicultural
multikulti
west african
jazz piano
west africa
flutes
world folk
mali
musica andina
jazz africa
multiculti
djembe
sufi
afrocuban
andina
saxophone
syncretic
jazz pianist
jazz sax
percussive
south african
afro-cuban
panpipes
underground hip hop
nigga music
hip hop instrumental
tweak ass shit when you are high
indie gangsta rap
compositions
ignorant but catchy rap
seen live twice
totally underlistened to
really shallow pop
music totally dedicated to christ
a mix of indie and hip hop
niggas that rock
crazy nice beats
awesome dance shit

music to-well you know
techno muse
this guys voice is amazingly high
emooooooooooo
jang geun suk
kdorama ost
metal experimental
neo-progressive metal
metal fusion
charango virtuoso
atmospheric doom metal
like dream theater
rock progressive
acoustic cover
technical doom metal
neo grunge
progresivo
progressive folk metal
gaita music
blond
quitter
good girls aloud track
baranyiz loves it
club classics
rolling stones top 500
christmas favourites
late night love
90s fun
barkbarkdisco
bark
jeffrey lewis
jonathan richman
post greatcore
adam green
so lo-fi
juno
modern lovers
bedroom pop
vaselines
concretes
solofi
emade
carnaval
sambas de enredo
british blues
new country
drinking
stand-up comedy
makes me want to die in the good way
emerging church
garage pop
math pop
falco
potf
simple minds
def leppard
sylver
blank and jones
depeche mode dm
ultravox
skid row
psb
eddie money
alpghaville
winger
dd
human league
hellogoodbye
tarot
royal gigolos
bomfunk mcs
reo speedwagon
guns and roses
men at work
whitesnake
steve winwood
lasgo
fyc
cello
laura branigan
tarzan boy
starship
bill idol
ian van dahl
johnny hates jazz
punk is not dead
horrible
girly-electro
monkeyrock
goodness
newman-esque
dance electro alternative
frantic ska
moonviolence
indiegoodness
cyber grunge
mdge
eeee
madoge16
madonnitaaaa
lo mejor
epically romantic
wedding dance party
good christian rap
instrumental alternative
black spiritual
wedding songs
train sounds
clapping
vaudeville
miracles
pro israel
stick it to the man
i love you
sub pop
punk funk
i wanna love you forever - jessica simpson
guitarist
pissed off musician
nevermind
in my life
michelle
here comes the sun
blackbird
title and registration
a
christofer drew
dumb and dumber
house md
the shield
battlestar galactica
kill bill
back to the future
full metal jacket
american beauty
good morning vietnam
lost in translation
the cable guy
dexter
tarantino
how i met your mother
final fantasy
groland
csi
the ark
jack black
the singles collection
girls are sexy
i love girls
my girls
express yourself
madonna copy
desnudate
get naked
inflated egos
boys suck
emotional pop
celebration
for my best friend
paradise kiss
air gear
soad
naruto
prejebeno
thrash till death
just could not get any better
metal woman fronted
jao bejbe
bolno
progressive rock -metal-
who is goth lol
could get any better
fuckin covers
industrial black metal
zadarski velikani :p
jebeno
black gay
fear factory
death-black metal
sinth
victoriandustrial
violindustrial
metal woman froned
fear factory cover
istinito
the beyonce experience live audio
patd
ryan
jon
new song
brendon
spencer
pretty odd
we are so starving
cities
colors
transport
girls names
numbers
musical genres
baggy
cops
originals
countries
days
scifi
bittersweet pop
boys names
cars
famous persons
food
questions
2 meter sessies
time
the best british album
blue songs
geography
violence
cuba
weather
postcore
fastcore
power violence
moshable death metal
electric blues
classic blues
country blues
boogie
blues n boogie
modern blues
jam bands
gutar
eclectric blues
blues guitar
early rocknroll
futuristic
mainstream artists that actually experiment with different genres and succeed
a decade of hits
futuretina
multiplatinum
neoprogressive
femme
neoprog
nice elevator music
female-fronted
gotanygoodmusic
girl singer
angelic voices
female alternative
spacerock
ambient pop
heavenly voices
fav track of a band
hey song youre so fucking cute
vocalists i love
love love love
root blues
steve stevens
billy idol
pat benatar
john foxx
asia
dave stewart
cathedral oceans
alphaville
ragtime


kim wilde

scott joplin
bozzio levin stevens
tears for fears
dream academy
syntpop
ladytron
italo
ministry
shut it down
jay sean
victor coy
microsoft
bill gates
legacy
portal
beyond
song of the decade
song of 2010
got it on 8 track
houston
kick ass drummer
jana hunter
indian jewelry related project
swans related project
unexpected sweetaddy cover song
title is a full sentence
nancy sings
8 track party tape
houston to baltimore
got the highlights 8 track
i own this on vinyl
got the quad 8 track
houston psychedelic rock
the greatest film-maker alive
wesley-core
real country
bosnian folk music
interview
poetry reading
cover tune
the last of the great early experimental current 93 albums
old school punk
bliss blood
todd rundgren cover
shearwater related
lisa germano
one of the first albums i ever bought
top ten of all time
midwest
diy
skramz
passionate hardcore
aussie
fockin cool
i fucking love this vocal
bossa no
neo-classic
favourite bands
demais
prog rock
mars volta
a blues prog rampage
far better than the original
fanfuckingtastic
incoherent
dissonance
psych pop
jerry cantrell
swinguera
bubblegum pop
60s revival
queercore
gay pop
50s revival
77punk
joey levine
birp
german-indie
horror rock
the best songs of the world
sweet electronic
iceland
vocal femenino
jovem
brasil gospel
nacional
brazillian rock
40 dias
baruk
fireflight
recordist
christian chick rock
christian women
danc
latina
musica
espanhol
ingles
cantora
i love this song
oc remix
concept album
underground rap
deutschrock
rock mexicano
latin ska punk
john peel
ritual ambient
red hot chili peppers
acoustic twee
al green
stevie wonder
hymn
rasta
grand funk railroad
the ascent of stan
dont cry for me argentina
haligh
bate cabelo
hidden gem
one of their best
wicked
personal
terribly beautiful
5-star
good memories
b side
5 stars
loved at first listen
should be played loudly
powerfull
strong lyrics
the best of metallica
harmony
precision musicianship
dreamlike
personal favourites
powerful lyrics
classic blackmore
personal meaning
fucking brilliant
hard as hell
hande
yener
apayri
albm
kibir
perfect lover
why should i be sad
amor meu
best band in the fucking world
leader
civil rights movement
funk-jazz
neo-americana
modal
cutting edge
contemporary jazz
big band
hard bop
post bop
organ trio
electrorock
jazz instrumental
iraqi
iraq
latin jazz
instrumental jazz
arabic hip hop
political hiphop
arab rap
basra
sax
tenor sax
bk jackson
julie london
amr diab
favs
arabic pop
lebanon
marriage
kathem
aktari
kadim
nazzar al qibani
carlos
ex-love
dulce maria
american pop
spanish e dance
latin teen pop
belinda
universal music
akon
madge
casi angeles
teenpop
undie rock

rorck
alternative e other
indie rock e alternative
dance party
vampire weekend
need to see live
inspiration
fantastic lyrics
ongaku
translation practice
anime ost
tchaikovsky
demolition is my mcr favorite song
its beauty n deep and sad
great geass ed xd
makes me cum
terminator
music for winter nights
cock rock
bergamo
loved tracks
omg this is so good
god damn long names
roque do demonho colorido
hardcore do mato
friendship
aliens
funny :d
title is so true
nfsu
relaxing music
classic jazz
diana krall
german synthpop
reaggea
roots reagge
chimezia
rootsreggae
reggea
back them x
roots ragga
femme fronted metal
christian rock-pop
nederlands
birmingham
charmed soundtrack
nice female vocal
rap-rock
contemporary christian
modern worship
tranquill
really talented female artist who doesnt rely on her sexuality to gain success
all-female music group
merge it already
censors are lame
floptastic
really talented female artists who dont rely on their sexuality to gain success
utada hikaru
tana loves her so much she would eat her up alive
trash
flop diva
wannabe singer
fake hair
deep ass voice
czech
so crappy i could die
come my way
penis
made of so much win it hurts
watermelon-watermelon-watermelon
disuko
in my fantasies i love you long time
close your eyes and imagine
optimistic view while experiencing pessimism
makes my soul tremble
rose
swan
words are useless
sublime mix of feelings
ahoy pirates have arrived
vocal house
electro-indie
stockholm
gotic
tribal music
great voices
britsh
bitpop
jazz female vocalists
ambient cello rock
approfondire
best unknown bands tracks to be observed
orgasmo
best songs of music i already own
riascoltare
blog
olimpo dei migliori
bunglish
intense sadness
things you should be ashamed of
so-so
semimerda
best bass tracks
songs to have sex to
perfect music for sleeping
zornish
far ridere i polli
rispolverare dal dimenticatoio
good for sleeping
horror soundtrack
to download
best drums tracks
weird al originals
laziness
high grade media
atlanta
intelligent techno
liquid apparatus
citizengreen
flexor81
phylum sinter
korg ds-10
bitcrush
mike cadoo
tay0
earstroke
improvisational
cevin key
phil western
the most music
threv
tricil
peachtree industrial
nophi
sunmoon
randomform
best music
daniel bedingfield
tektonik
indie-funkrock
meat loaf
teena marie
westlife
air supply
meat loaf will fully smash you
lyrical brilliance
lisa stansfield
cover version
az yet
flasdance
western
china
group
berep shows the love
check
mistagged artist
mistagged track
berep guest dj closedmouth 239645
instrumental music that makes you feel-group tag radio
i am a party girl here is my soundtrack
berep guest dj closedmouth jfr
heard on 30 jul 06 downtempo sampler radio
gladys the tag
out of order
downtempo group favourites
mooog sings
downtempo custom
boredoms-related
9100
8555
yoshimi
technical progressive death metal
not math rock
improvisation
mistagged album
8500
bad stream
9111
9000
wicked beat
4700
8888
7100
10101
7800
7500
8600
prog metal
progressive melodic death metal
gothenburg
dunno
insane keyboard player
finnish death metal
insane guitar player
symphonic heavy metal
prog heavy metal
sturgiscore
hardcore br00tal
burns
instrumental metal
mindblowing
sex for my ears
sludge rock
i had to change my pants after this song
rnb remix
angel
perfectshotstv
sxymodel
thickgirl
thickmodel
sexyredbones
sxyyellowbone
cyber-core
space music
blyaaaaaaaaa ya ebal
anal vocals
anal destruction
atheistic
russian indie
anti-church
russian pop cover
apocalypse
cyber-indie
highest note of matthew bellamy
psyshit
chaikovsky
gypsy punk
dinking songs
good cover
muslum gurses
agir abi
pit10
quran
ottoman
qari
rahmanin bulbulleri
kuran-i kerim
tilavet
islamic
coran
kari
hatim
egypt
hafiz
yasin suresi
turkish classical
kehf suresi
le saint coran
kelamullah
holy quran
saudi arabia
dream trance
morocco
ilahi
zikirli
dhikr
dhikirs
zikirli ilahi
hafez
nebe suresi
duha suresi
albums i need
albums i have
love this girl
autotuned
assmo
no homo
grad school
pennsylvania
ex vaseline
ex raychaelray
cock n roll
pro-negativity
anti-positivity
post-shit
douche music
dinky
bros gone wild
brodacious
bro code
bro-talk
bromance
ay ya ya
piriguete
best music of 2011
pedro castro
luxuria
fun fun fun
anette olzon
the rasmus
diablo
outta my outta my head
sharon
keith caputo
best album of 2011
old school industrial
marinha grande
agrometal
pimba
fado
german hip-hop
drug
lunic
kaitee page
sellaband
tori amos
song
alanis morissette
flyleaf
metric
apocalyptica
pj harvey
megan berson
arcade fire
company of thieves
uh huh her
vanderbilt
ra ra riot
acme studios
yeah yeah yeahs
versaemerge
shannon wright
kate bush
dresden dolls
bat for lashes
the airborne toxic event
blondie
high energy
blood red shoes
lunik
lovethief
especial
latin american
the srokes
strokes rule my life
discoverockult
getting over
linger
thought
number one
breakup
crush on a friend
what you do
crush
soulmate
divorce
finding your way
pierre bouvier
pete wentz
uptempo
uk pop
uk indie
pop indie
macy gray
cold play
grupo
sean paul
cristina aguilera
brithney spears
cool jazz
wes anderson
instrumental funk
danny check out
tom sharpling
earlie indie
guitar blues
bluesrock
blues guitar women
haterscanstfu
fo real
he gon find you
hide ya kids hide ya wife
climbin up yo windows
friday
fun fun fun fun
robot unicorn attack
free music worth paying for
fun music
soundtracks
electro-dub experimentale
neotraditional country
anointed
hated but rated
tradtional country
cheesy pop
i like it but i dont know why
hardcore country
quality
shockingly good
best artist ever
western swing
good time music
country to the core
new-traditional country
grand ole opry performer
the word of god
new traditional country
genuine country
foot tapping
filth
tease
calippo
90s childhood nostalgia
rocking
great rock-out song
guilty tone deaf pleasure
christian alternative rock
dream-pop
brit-pop
space-rock
math-rock
noise-pop
experemental
castrato
floor
toes
across
jetpack music
catwalk
refreshing
praise
favorite male vocalist
die aerzte
die beste band der welt
frenchcore
billy talent
straight line stitch
him
the dream
in fear and faith
female fronted band
wi
withered illusions
acoustic session
the lady is not for turning
mingle rap
post-grudge punk
country-folk
alternative indie
unorthodox pop
j rock
oshare kei :
visualkei
visual kei n-n
rock indie
j rock plastic tree
eletro
rock japanese
jrock kawaiii
jpn
j rock indie
gt
gothic japanese
sww
sdsd
j rockock
c-rock
kih
an cafe
kj
tdf
ienfe
j  rock
fg
love roger
j-classic
lindoo
efuckkkkk
dw
iceland bjork
ojsh
fv
incubus
placebo feat david bowie
jean- michele jarre
jean-michele jarre
bronski beat
frauen und technik
ladytronica
relaxing muscles
tangerine dream
im morgengrauen alleine u-bahn fahren
best croatian pop-rock band
soul-pop
d'n'b
wide horizon
inflight music
peaceful ambient
limehyperambient
outer dimensions
ambiofusion
light ambient
futureradio
future structures
sonicwire
ambiolectronic
subsonic harmonence
deep electronic
neoambient
mind music
eyes wide open
catalan
euskera
orchestral metal
metal opera
cape verde
pink floyd tribute
nikki sixx
hair and glam metal
hair band
saint of los angeles
80s forever
my gods
80s rulez
vince neil
mick mars
tommy lee
motley
too fast for love
altar of the metal gods
o-baby
lovely hair metal
vinnie vincent invasion
nikki  post crue
tracii guns
hell
true female rock
post-thrash metal
pokecore
top christian
diego 12
jesus 12
diego
adoracao
diego12
cristian rock
dieo 12
perfecttt
awesome voice
rockstar
lullabies
rock e rock psicodlico
rock psicodlico
glorious tracks
best mixes
electronic junk punk
mute records
omgwtf
exhilarating
calming
skate punk
travis barker
satine rock
groundation
the classic crime
emo-metalcore
matisyahu
bullet for my valentine
vogue cardiff
kiss me baby
mariage blanc
pittsburgh
delicious pastries
olivia tremor control
minneapolis
snack music
sleeping in the aviary
beach boys
new shouts
oranger
teenage beach sounds
ringpop
herman's hermits
alex chilton
satin gum
the troggs
harry nilsson
elliott kozel
fundip
apples in stereo
arm salad
the gerbils
big star
meeting of important people
sparklehorse
ronettes
urban winter
donovan
lateduster
cepia
space lasers
philip glass
fat kid wednesdays
public pool
marian
walker art museum
tarlton
out for blood
jt bates
poor line condition
huntley miller
wimpy disco noodles
moses
brett bullion
urban winter folk
danceee
brazilian electro
eletrorock
skyyyyyyyyyyyyy
walter meego
rycassssss
baladinha de final de tarde
dick man
ricaaa
deep cuts
do0paunocu
belissima
loucuraaa
artmaker
traveca
fucking perfect
best of year
really queen
mumia
tutancamon
flop queen
hora de ser mae
cachorra do funk
vagisa
young diva
nu rave
deep techno
minimal teccno
progresive trance
minimal house
make me cry
harmonic
harp
whisttling
layne staley
mark lanegan
polish punk
intelligent
great music
love japan
tecktonik
hc ljubav
filmska muzika
hc ljubaf
hella diy
favorite band
indie-ish
ambient folk
my favorite song
best song ever written
rad cover
best rapper alive
artsy folk-punk
chris cornell
serj tankian
dave grohl
daniel johns
wesley reid scantlin
humberto gessinger
selena gomez
rolling stones
keith richards
videosex
ex-yu
five strings two fingers one asshole
keef the human riff
frusciantism
john frusciante
slowies
collection
debut album
edit
song with samples
ztt
liverpool
trevor horn
drum attix
wishes he was brian wilson
skits
paola mirabella
bete
lunettes
oculiste
puzzola idiota
antifolk
freak-folk
bossa
post-folk
vibey
weird acoustic
new american weird
catania
ethno-rock
freak-pop
pachamama
subterra
anzio
freak folk tropicalia
anti-anti-folk
duckhead green
rocketta
tommy
p-birdie
honeybird
ginobird
buzz
gardening
lake
bees
pink lemonade
snapple
sticky sweet
quemby
pollock pines
limone
talia
siculo
carfagna
catanese
better than kmad
vyjeta ropucha
nice sword gandalf
astroubrus worship
home collection
over 2000 listeners
non melodic
virgin metal
maracaibo
mujer de la nuche
bond
2 tone
welsh
jamaican
f
egyptian
skeet surfing
my loving
punkwave
minimal wave
minimal synth
baile melancolico
french wave
euro disco
minimal elektro
elektro punk
katamari damacy
she wants revenge
information society
synthtrial
vnv nation
visage
russian synthpop
mesh
merge
front 242
sythpop
depeche mode sounding
emmon
marsheaux
parralox
la roux
good mood music
good mood songs
infatuation
childhood memories
breakup music
breakup songs
because sometimes i feel like a 13-year-old teenie bopper
sometimes its just about the hot singer
music by friends
kate griffes
corinne bailey rae
dark songs
unplugged
the cardigans
high school memories
music that reminds me of friends
edgard portela
live in dublin
raul midon
andrea corr
kt tunstall
mana
songs i can play
ten feet high
sexual
young guns not young gunz wth
letmelikeit
lmli
check out later
rockford rock
sugar pop punk
chicago emo
lifetimecore
easygoing days
nintendemo
ride the bus
hardmo
emo hardcore
oozing wave
roselle hardcore
spaztic hardcore
pop singles
pop covers
jimi hendrix tracks

bahia
ivete
dalila
dance disco eletronic pop jennifer lopez red one pittbull
lixo
piriquita
infantil
anos 80
chingocore
overrated hipster garbage
i used to like this ultra shit band
chugga chugga chugga
black metal cabeza
holy fucking shit i came in fucking buckets
i came
albentcore
gabbacore
nadacore
holy dick everyone is going to fucking die
gayest song ever
mainstream music that doesnt suck
sida
caca
babymaking music
avoid this music at all costs
pure perfect hate
dat riff
lolcore
regressive post-metal
post-music
dat bass
sellout
lol sounds like joy division
sida para portenos que no quieren viajar 2 km chotos para ver fueguitos artificiales y se quejan
implying this is prog rock
post-ironic beardcore
burpcore
four walls and adobe slabs for ma gurls
elforrocore
hangover
fußball
anti-messi
flac
corecore
joa
lapiz conciente
rap dominicano
elcriminal03
vakero
toxic crow
lapiz conciente vakero hip hop latino toxic crow rap dominicano
regueton
polaco
eldad
for work
tamir
irish music
motiv
shoshi
gadi1
drive
tango nuevo
gibrishh
north africa
shine on you crazy diamond
roger waters
nature sounds
easy rider
para ensear en ingls
beethoven
joy division
house society
flirty
europe
cretu
gay as hell
alizee
russian gay
europa
patricia kaas
mylene farmer
cinema bizarre
deutschland
eurobeat
british 80s
disco never dies
dance mix
tv themes
audioslave
the tea party
dance with somebody
dreams
acid house
marimba
musica negra
pacifico colombiano
chaos
indie sinewave brazil sao paulo
recommended
oriental rock
quiet mellow tunes
ompa
recommended cell phone ring tone
uber thoughts inducing
driving music
gypsyrock
working music
highschool music
better than metallica
melodia
especiales
the crow
dimebag
oldschool
gordo wallas
walas
funniest dude ever
cool as
cover versions
stupidly quick talking
zorro musical
mairead nesbitt
musical theatre
kabardino-balkaria
sissel
musical actress
actor
cho te nado
the most talented singer
alexander rybak
secret garden
double platinum
beauty and the beast
ensemble
chastushki
under 100 listeners
power-pop
child singer
zorro
mamma mia
anuna
pop-opera
christmas carols
operatic pop
fucking lovez it
007
cornell
aguilera
yay obsession :d
dick tracy
remember the time
christina
afro-soul
whistle
depress
tirar da fossa
british soul
sadness
musicas lindas
escuta isso por favor
dupla
pra acalmar
enstrumental
murat esmer
turkish rock
the ewox
odtu
depressed
unrealistic phantom
fight club
etnik
protest
rust in peace
rage
turkish melancholy
gerzek
wolrd
sensefield
braid
hot rod circuit
hey mercedes
the promise ring
texas is the reason
face to face
lifetime
samiam
sunny day real estate
the cars
jimmy eat world
the velvet underground
one hit wonders
deutsche welle
girl punk
female empowerment
life of pain
the fucking best
good bands
floating
samba bucolic
aussie rock
brazilian indie rock
baroque harmonic pop jams
vintage pop
just love it
dj sets
collie buddz  mamacita
collie buddz
male version
pop-rap
melodifestivalen
daniela mercury
south american
music and lyrics :p
one of the best songs in existence
the greatest rock band of all time
damn i love this fucking guy
mr bad guy
indonesia
1920
bandung
drinking songs
beer metal
anarcopunk
music to listen to by a forest river at night
songs to beat someone
songs i cannot live without
too long but genious
your ears will bleed
crime against humanity
cancer thats killing music
musica catalana
1966
1965
1967
1964
1968
1963
bach
relax del chido
debbie davies
female blues vocalist
blues women
female blues
jump
blues harp
pnk
pittsburgh roots
joanne shaw taylor
guitar girl
the who
badfinger
fsu
the american milwaukee straightedge 2k10
liam gallagher
pl
polski rap
love forever
polish rap
summer 2010
flo rida
kocham
polish hip-hop
polski hip hop
female rock
ja tu tylko sprzatam
ewa farna
earth-shatteringly sexy music
my favorite band
sweeping
text
guter text
breaking benjamin
3 days grace
movie soundtrack
lyrical rock
chevelle
godsmack
rishloo
stone sour
gute text
staind
red
chorus
good lyrics
german rock
12 stones
soundgarden
karlsruhe
nachdenklich
cool rock
alter bridge
perfect circle
vocals
trip lounge
sneakers
rap hip hop
good old times
lpa
great memories with my siss
tbu
oh so catchy
great memories with my geeem
songs that remind me of summer 07-08
these songs are just amazing
great moments at et7
songs i sing with my mom
viaje de egresados
heard live
songs to make florchuchizz cry
makes me smile like an idiot
wake up song
so gossip girl
caaamiiiliiitaaa
amazing music i listen to with my daddy
this song hits really close to home
best love song ever
barney stinson get psyched mix
sitar
engrish
tops
depthcore
inditronica
critic
politic
indie metal
pregressive house
color
cut off
cuerdas
distortion
lo fi
satanic
anti-christian
finish metal
gods of metal
avantgarde black metal
french black metal
atmosferic black metal
polish metal
monster metal
devil music
sweet country-folk ballad with  a tom waits twist
great indie-rock track with strong vocals
beautiful and haunting singer songwriter folk-pop track
3 fat pills is an anthem of debauchery
quebec
francophone
mc
acoustic reggae
pop with groove
indie rock pop melodic francophone hooky catchy solid songwriting
canadian folk pop singer songwriter rock acoustic
excellente hymne a la vie et la nature
excellente chanson sur la pate a dent
gorgeous world pop track
tribal womne of the amazon rocks my world
cool mano negra cover
one of the best songs ive heard
killer rock song
welsh indie rock in your face
wales rocks
fabulous melody in this indie rock gem
hooky rock anthem of cynicism in the media
absolute bomb rock-punk track
the truth hurts and george bush sucks
this song rocks
indie pop beauty
great pop with real soul
tom waits takes a trip to the saguenay
great indie-rock song
what they worse live
hipster garabe
brothersport
personal hero
dream funk
smooth funk flavored milkshake
female singer songwriters
girls with guitars
girl rock
chick rock
feel like dancing
furnoo lovers
boy :d
best rock
jochos
teen pop rock
full
ich liebe dich
ja comi
as i lay dying
ghost brigade
in mourning
jams
80s stuff
reba mix
oldie but goodie
deeeva
vocal couch
hdk
razorblade edge
violet
eve
i want in my bed
verynice
k-dance
so sexy
nic
bb
k-rap
asian pop
hime
korean hottie
cutest thing ever
best of all time
best of 2011
b
princes
dearly
better than lady gaga but i love it too
one by one
we gonna love one another
rare
-q
sunday morning
calssico
classico
best single
cody simpson
miranda
maite perroni
warldof
meester
blair
leighton
gossip
american single
miranda cosgrove
icarly
energy pop
beautiful woman
simpson
ride
bumpy
love generation
nobody
redone
mohombi
loser
fail
recalque
neide
neyde
playbackney
hiamfail
lixototal
best albums of 2008
paula abdul
lastfm
alternative  punk
klezmer
electronicadance
general alternative
post-rockexperimental
miscellaneous
nu-post-awesomecore
inconnu
rockpop
santa fe
martian boogie
hollywood star
fire and the holy ghost
they slew the dreamer
jongleur music pictures
jongleur music
i give you praise
he is god
mlk
dreamer
santana
r & b
rock & roll
movie
michelle williams
teyana taylor
scream
atreyu
adult alternative
avangarde hip hop
2-99 records
apokriff
all the best
best songs of the 00s
best songs of the 90s
best songs of the 80s
best songs of the 70s
best songs of the 60s
best christmas songs
best songs of the 50s
the best classical opera and film scores
oscar winning songs
vh1 100 greatest albums
best songs in the world
best of albums
best songs of 1900 to 1950
movie soundtracks
fab albums
songs
movie songs
my favourite soundtracks
raven girls top 20
wales
songs diana sang on x factor
tracks i want to see on lastfm
cult movie songs
tribute albums
new discoverys
songs to listen to
storytelling
singing actresses
tv soundtracks
singing actors
partytime
comedy songs
listen to
future stars
eletric
mp4
nego
tutz tutz
the clash
polish folk
fuck you i am from poland
american idiot
brodway
bands from las vegas
makowiecki
jack white
female rock band
if my soul were composed of musical notes
monstrous
total fucking win
pure beauty
im taking off my clothes too paul
bestsongever
ambidjent
blink punk
misanthropic black metal
extreme black metal
sophisticated black metal art
awesome songs
techincal metal
death metal pioneers
smooth piano
very nice solo
soloage
interesting music
british black metal
american black metal
music that makes your head explode of amazement
punk covers
bad day
life on mars
titans
donna
bobbi
roy
garth
clint
simon
wca
babs
outsiders
wally
lily
runaways
rachel
raven
government
caleb
dick
tula
ya
tony
jason
kon
amelia
jenny
tara
ollie
suicide squad
sand
corrine
bad days
unsure
vic
bart
kyle
anya
dinah
x-men
roxy
bad girls
kathy
jesse
birds of prey
namora
teddy
garden state soundtrack
six feet under soundtrack
walk the line soundtrack
south park bigger longer uncut soundtrack
alfie soundtrack
everythings illuminated soundtrack
gta vice city
batman returns soundtrack
neon genesis evangelion
greys anatomy soundtrack
moonlight mile ost
the last kiss soundtrack
gazda paja
ljubomir sedlar jazz funk
pure cane sugar sugarman 3
funkadelian
djuskaj kupatilo boban
smooth talk evelyn champagne
funk soul rock sly larry jerry
quannum lyrics california
uncle jam one nation clinton
bootzilla rubber duckie space bass
hexstatic ninja tune masterview
p funk george bootsy bernie gary
dj food ultimate funk groove
babyface manchild 70s
heron
esse quam videri mordred
rony playstation
pure groove
tagged indie just for the sake of it i guess
sbornia
gremio rock
conjunto
acordeon
milonga
gremio
benga music
benga
atonal
the fucking greatest band to ever grace this pathetic planet with their holy presence
andre abujamra
ni
eletro-pop
brazillian
viollet
duffy
bom pra caralho
lovely gaga
j.lo
iron jesus
fucked up
brain dance
gescom
this will make you shit your pants
wierd atmospheric
afx
raymond scott
morton subotnick
strange-core
wierd-groove
moondog
mind-splat
delia derbyshire
mental
venetian snares
coil
the tuss
steve reich
tod dockstader
head fuck
chris clark
head groove
clark
joey beltram
avant garde electronica
rezanie psychiki
heard on pandora
new vicious
dejeto
natal
atoron
fix your tags
natal-rn
needs a sandwich
beautiful video clip
break my heart
victorias secret
oh my god
good to listen to while driving fast
bald
riot grrl
listen at night
indierock
post-hardocore
gooncore
country metal
straight outta compton
melbourne
anarcho punk
overproduced by martin hannet
i bet he does
dogcore
seen die
kroegercore
bi daha dinlicem
bu grubu hic duymamistim
drug music
aman tanrim
drug song
sonra dinle
ara bul
boa musica
voz marcante
grande banda
as tags
musica boa
uma grande banda
maravilhoso
volte
clipe sinistro
as tag
melhor compositor do brasil
bela
que musica boa
very cool rock song
musica realista
linda musica
que coisa meu
que coisa
grande musica
ultra musica
fantastica
muito boa mesmo
tagas
perfeito
yes
shakti
john mclaughlin
edgar allan poe
chic
directions
sibelius
many
mahavishnu orchestra
straight
william blake
robert frost
flatpicking
edwin
etude
villa-lobos
plectrum
gnosis
progressor
composing
heartscore
dirk radloff
tim warweg
stephen crane
henry wadsworth longfellow
oliver hartstack
john crowe ransom
arlington robinson
bad homburg
dissonant
langston hughes
art-rock
hot guys
bodhran
irish traditional
fiddle
low whistle
heather lewin-tiarks
amy richter
jeff ksiazek
milwaukee
wisconsin
chicago
ceili
andreas transo
parker otwell roe
taolchemy records
patched hats
athas
kathleen bremer
concertina
pipes
matt hendricks
musician
accordian
slip jig
late night tunes
study tunes
bard metal
progressive indie metal
wish i could see live
fusion alternative
wolf
funkadelic
driving tunes
awesome metal
cheese
virtuoso
late nite tunes
progressive power metal
brasilian
fake
male singer
its friday
virgin
female singer
new media
harmonisch ambient classical new nu classic
more
harmonisch
melancholisch
random
nu
indie electronic
buy this
smooth night driving
coffeeshop
softspoken
whimsical
kicking
finale montage
wake up
quiet walk
beach
driving victory
finale music
alluring exotic
cello rock
finale
paint-the-world-in-colour
supernatural
human spirit
dark alley
kill billish
rocks like hell
murder ballad
rhythm'n'blues
poor boy songs
pre-war blues
cute girl rock
hotrod
swings like hell
saxophones
floorfiller
beatiful
anathema
killswitch engage
croatian
peace punk
anarcho-punk
rise against
notag
loved by archer
bard
instrumental blues
bard rock
menestrel
bardic
russian underground
lyrical acoustic folk
minstrel
free mp3
rpg music
alchemic rock
rolesong
acoustic blues
blues rock guitarist
blues guitarists
uuaaj
papillon
running song
bushoi-tiptop
jamie t
iwasrecommendedthis
songs that i will still be listening to in a billion years
feeling of happiness
uk top 40
ubernaturatiolastrabostualism
rock night songs
totaly awesome
great video clip
kings and queens
songs that you wanna listen to over and over again
das rockt
kings
epic music
awesome guitar
experimentell
postmodern
outsider
gay electro indie pop
popculture
gay explosion
sakral musik
singer song writer
magick ritual musick
tekmo
heyyoufuckingman
flaw
30stm
echelon
don't forget
love her so much
i can do better
porno-soul
happy post-rock
songs to feel happy to
queer as folk
listen while crying and feeling like shit
britpop 90s generation
haired-red fronted woman
brega-rock
cute-cute
weird-noisy female but good singer at all
depressive synth
dirty punk rock
christian power metal
metal sisters
ska pop
darkambient
unanswered
sad lovely touching expressive
canadian heroes
beautiful piano
bladen county
ripping
portland
spazzy
fresh
as apimentadas
i am hot
tisdale
ashley
bring it on
think
hairspray
light pop relaxing
celtic pop
medo
darkness
kimberley locke
i could
nerd rap
radiohead office chart
acid rock
etnic
waltz
seitkaliyev
smokeyblues
heartswelling
exquisite
enchanting
hmm
the lights
irresitable
memorable
ponders
bitter-sweet
electrohop
reallygood
katy
liz lee
my life as liz
paramore still a band
song for katy perry
dixon
volcals
two step
g-e-n-i-o-s
g-e-n-i-o
viaje sensorial
volar
latinoamerica
arte
viaje mistico
viaje espiritual
rock latino
mas grandes que jesus
mágico
united states of tara
major tom
funk to funky
la cancion de mi generacion
mt bao
carah faye i love you sua linda
beyond alive
dark trance
uk hardcore
synth rock
fire with fire
scissors sisters
ameni swissi
christina aguilera beautiful
amani swissi wain
freedom
hora
elfen lied lilium
kelmti hora
amani esswissi
amani wain
kelma
amani swissi star academy
lilium
lilium long
amal mathlouthi
amel mathlouthi
lillium
kelmti horra
lilium extended
kelmti
horra
freedom of expression
lilium very long version
7orrya
horrya
lilium darkjedi final
yelle
theatrical music
mobb deep
busta rhymes
70s punk
guitar classic
digital signal processing
ghostface killah
epic symphonic metal
symphonic melodic power metal
axel ritt guitar shred
glados
johnny's
brainstorm
mitchel
musso
hannahmontana
mitchelmusso
the fame
safada
brand new artists
ultimate crap
favourite female artists
saw them at dar constitution hall
saw them 3x
must see live
new grass americana
blues slide guitar maestro
rn
minimaltechno
whoooooosh
strings acoustic
blues band
industrial goth
not much of a singer
listen free on lastfm
low-fi
undiscovered
an enigma
have seen in-person
bizarre at the time
once had the vinyl
post moby grape
out of his mind
nasal vocal
hypo-tripno
imposter
no depression
belleville
tweedy
farrar
hop aboard the spaceship
randy california
still have the vinyl
jay ferguson
swedes showing off
ziggyesque
m is the man
friend of grizzly bear
more great music from brooklyn
old man
used to play feel a whole lot better with a garage band
original member of the byrds
performance art
indie hair rock
pierre et gilles
canadian indie
that fucking tank
this et al
downdime
the lodger
wild beasts
benjamin wetherill
kill
yourself
on the bone records
burnage
regional mexican
nederlandstalig
wfmu
this song tells the story of my life right from the bottom of my heart
the essential leonard cohen
bs
silversun pickups
dig
so fucking good
rumba
most favorite song ever
all time favorite
funny damn shit
jim brickman
brooks and dunn
male country
idie
sxsw2006
new album loves
angels and airwaves
blink 182
foals
fucking adorable
flamenco pop
x-factor
junior songfestival
rumbapop
boyband
rumbita
bachata urbana
soul diva
melodic pop
rumba-rock
minimalist pop
jaw dropping
florence and the machine
cockney
boppin hillbilly
hot rockin girl
rockin hillbilly
hillbilly boogie
rare rockabilly
wonderful doo wop
chicago rockabilly
doo-wop
jive
country style rock n roll
nashville rockabilly
doo wop jivers
neo-rockabilly
white doo wop
british rockabilly
roots of rockabilly
canadian rockabilly
hillbilly music
boston rockabilly
memphis rockabilly
rock n roll jivers
hillbilly harmonie
louisiana rock n roll
boogie woogie
countrybilly
sun rockabilly
southern country rock
country roots of rock n roll
country billy
rockabilly women
ambient techno
power noise
mambo
kumbia
amazon
99 most essential
neo-classical dark wave
experimental techno
sweet bands
new traditionalist
mood music
toytronica
ace of spades
songs that i love
sarah
poesia
aumenta o volume
classical pop
classical-crossover
a-teens
josh groban
moloko
sarah mclachlan
alternative in the 1990s
1 giant leap
moby
frou frou
chill house
weekend players
electronic dance music
80s i like
la academia 4
eastern europe rock
nk
christian chavez
chochi
petardeo
valencia
garfios
vicente sabater
actriz
piratas
people do envy her
teen diva
role model
revelacion 2009
chica fabulosa
chica pop
histerica
oversized heads
yurena
electroperra
ambar
toronto
o rappa
roots and culture
duos
rastafari
enghaw
fab four
abyssinians
crucial reggae
regional
gessinger
engenheiros do hawaii
bossa-nova
pulsating
woodstock
amelie
yann tiersen
catholic
catolica
religious
bolero
multiple vocals
brazilian axe
rock music
precious
awesomecore
zydzi
twoj stary
zajac
pernambuco
favorite singles
favorite singer
best of 2006
j-music
best of 2000
jmusic
c-pop
cpop
chinese music
rockish
children
fort minor
eagles
fromhell
sweetcore
gaucho
intimate
hard to explain what i feel when i listen
techno-pop
female rap
square one
cant be tamed
huge artists
upcoming hit
slutty
hogata
jigsaw
bjonik
body control
headstrong
the pretty reckless
blender
italo-disco
trombone
true legend
artists to check out
club music
bergen
cooking
food writer
scandi-pop
acoustic chillout easy listening
scandipop
female rocker
underrated performer
favorite rock band
sexy talented
caibo para que vuelvas sandunguera maracaibo pop
jota quest
mbp
skank
enrique iglesias
last unicorn
classic punk
make me feel better
solta a franga
make me crazy
fucking good
best voice
drunk
gay music
i fucking love cassie
lesbian
deep-clash
s-r-b-i-j-a
in the witch house family
witch chill
dark disco
anti-witch house
deepclash
broken-clash
in the chillwave family
newbreed
electroclash ingredients
chill witch
witch-clash
neo-electro
deconstruction
post-electroclash
factory records
zombie rave
drag
post electroclash
neo-pop
too cool for normal tags
microsound
gan-radio
deep-electro
glo-fi
electro minimalist
cult
serbian jazz
new electro
e-l-e-c-t-r-o
slut wave
gucci goth
chill wave
capoeira
windows
smth strange
gop style
siberian punk
requiem for a dream
capoeira angola
batucada
mystery
fatal song
classic russian rock
and i ask
0 play yet
cesar
rafa
hans
more plays please
thalita
mauricio
tanguy
vitor
jr
tomcardo
disney girl
the probabily music of my marriage day
ma dream came true
whogavethisbitchthisvoice
henrylaurocks
classic hard rock
ac dc
musica popular brasileira
forever britney spears
limds
the best ever
very sexy
my favorite hip hop artist
jerk music
screamo crunkcore shit
electro-hop
persian love
french electronic
inspirational shit
this shit fucking rawks
real fucking music learn from it i love you mescudi
sky so big that it broke my soul
ive got to breathe
one of the thousands of things we have in common
i want you too
i love sex fires
my hot girls
makes meh cry my eyes out shit im sad
powerpop that makes meh fuzzy inside
i fuck to this so its chill
electro indie hip hop
gay shit i love pshyeah
geek rock comedy
kinda country super sexy
insane
sick minimal
penetrating sound
cumm
faggotari
thisissick
dc
michael buble
how deep is your love
bee
dougie dont go into the woods
hard electro
rock alternativo
carnivore
lembra alguem
ashey tisdale
kt
reminds me of a special person
female vocalists - brazilian
pitty admiravel chip novo
slamming brutal death
aliguka
rapper
sunday drive time
glory
german punk rock
journalist
bigband
the blues brothers
mundart rock
popcore
indie balladen
a capella
ratzeburg
proll
karl
arne
bollo
anton
martin
forever
family
popao
belle sebastian cover
little miss sunshine soundtrack
birthday song
generic
dance punk
and i hate that my feet are dancing so much
unppluged
the stone roses
velvet revolver
queensryche
khigh
backyard babies
sound garden
faith no more
zakk wylde
black label society
ricky martin
hoobastank
rob pattison
robert pattison
robert pattinson
edward cullen
crepusculo
hayley
craig david
ignorance
chris daughtry
i love when i listen a7x
bleeding through
sonic syndicate
monstrum
electro girls
czesiek
suxx
disco punk
miazga w hu
kury
disco fuckin yea
you are welcome in poland
lovelskie indie
kocham cie
debilcore
miazga
daj kamienja
off
fisz
muzyka dla mietkich chlopcow
elwis
yeah fuckin yeah
pank
rege
klubowa
polish death metal
pldm
kinder
mallcore
slam death metal
deathgrid
rock patriotyczny
wszystkiego po trochu z przewaga napierdalania
not death metal
hyperblasting technical brutal death metal
bass virtuoso
technical black metal
crustgrind
this is not deathcore
godspeed you! black emperor
red sparowes
god is an astronaut
labirinto
explosions in the sky
neurosis
isis
pelican
this will destroy you
russian circles
rosetta
white funk
arkadi
bazarov
east
rock progressivo italiano
prog folk  romania
ilka
atelier
anja1992
werner-seelenbinder-halle 1989
greece 2004
anja
itssowonderful
crap music--delete this song
richard dixon radio
more crap
keeper
best keeper
best around--dicky2 u
piano pro
christian blues
richard dixon  radio
red hot   love
roger was the ---king of the road--
alternative rock  gooood
buddy left us way to soon
opened a show with bo years ago---him and a bongo shaker--had a fantastic sound--fine showman--dickyd
kitty wells is still the queen----dicky d
sick sick sick
delete
delete artist now
unplat
tiger
red hot chilli peppers
overtone singing
similar to dam   anti-war   peace not war
music for life
tricky
supernova
omg
sister michael jackson
trent reznor
british invasion pop rock singer-songwriter classic rock british
oli sykes
wa
pa pa pa poker face
fantastic vocal
survive
immortal
killed
my favourite song by michael jackson
quinsy johs
dandy
featuring kanye west
usher
simply beautiful
80
various
big hats
show
songs that make me cry
hip
my favourite albums
indiepop
sings like liam gallagher on a good day
wonderful
my favourite suede songs
best fucking song with organs that makes you want to jizz all over the place then clean it up and say rofl
soest
hip-hop latino
pedro me da meu chip
sandy e junior
phodastica
emofuck
xyuta
ghost
dr house
melancholic synth-pop
progressive pop
austin drage
ruth lorenzo
alexandra burke
somebody
ambitions
vocalista feminimo
musica russa
grupo feminimo
grupo masculino
can't live without
make me over
heatbreaker songs
the best voice ever
pure awesomeness
neo soul
neosoul
rox
the vampire diares
katarina graham
bonnie bennett
sketch
robot rock
makes me wanna dance
youth
skacoustic
everyone high-fiving everyone
bleep-bloop punk
tumblrcore
hurry up and save me
justin bieber - my worlds
anal princess
eletro pop
south represent
boyz ii men
songs from adverts
singalong
i am in love with this song
hyper
i love dancing about to this like a complete and utter idiot
soon to be classic
across the universe soundtrack
album favourite
harry potter soundtrack
addicting
piano led indie
soundtrack to my life
break up
romanic
i want this played at my wedding
indie pop punk
favourite song ever
sertanejo universitario
led zeppelin - stairway to heaven
led zeppelin  - immigrant song
apc
aeroelectronic punk
omg original in u2 :o
la lluvia xd
road stripes
que te valla mal mal mal cuando me veas -glee apestoso
them crooked vultures
medicine
trash electro
shinee ringdingdong kpop ring ding ding k-pop
japaneseben
this light
tobias sammet
ghost opera
avantasia
karma
goa trance
black halo
rhapsody
gms
skazi
1200 micrograms
1200 mics
this light - this light
dark psy
darkpsy
raja ram
songs that rocked my world :p
stripper music
secondary school favourites
songs that never fail to make me feel awesome
songs to hear before you die
songs about me
puddle of mudd
electro prog
indie luv
neon space
my electro feelings
idolo
inlove
remember him
rock me babe
i love my dad taste
just chillin
hoy tengo ganas de llorar
i want to dance in my underwear
q sexy
es pop mama
skream out loud
riot boii
oh electropop
world beats
acoustic emo
lor
bruno mars
luca
new kids
maroon5
backstreet boys-you are and will be forever my life
lorita
poem
rhythmic
lou reed
classic rock - the doors
jobim
touch my body
craig
not human-conceptualized
1972
abraham son of teraj brother of najor and aram inspired music
wonkylectro
future
lo-fi-folk
polyphonic
desi
drake
young money
quiet  music
short song
makes me feel good
an
zombieland
hot like mexico rejoice
diego torres
waldman
pablo milanes
niche
sin bandera
silvio rodriguez
gente
me gusta
nueva trova
juanes
bloc party
wonderfull
jj ackles jensen
snow
let
it
wellcom
gimme
anna
felix
nie wstydz sie mowic ze kochasz
nowakowska
gasz
zosia
michal
piotr
rubik
honor society
nine crimes
akustyczna
shannon noll
maccabraa
bednarek
kamil
jen titus
margaret
happy rock sunday
nightwish
albums i can listen to a million times over without ever having the urge to skip a song
crimes against humanity
someone in my family is famous
slide my wide rubbery tallywacker into your puckered vending machine
rapes my ears
worthless piece of shit
stupid fag
worse than crazy frog
i would rather beat myself to death with a hammer than listen to this
wannabe
the worst thing ever to happen to music
hiv positive
all things annoying in the world put together into one stupid bitch
kill me now
bad music
sexy album covers
trainwreck
paktofonika
chwile
ulotne
rules
durutti column
this heat
jon wurster
tom scharpling
fave songs
femal vocal
fave singers
impressive vocals
emd
red one
upset
typical
having
emotioal
melanie fiona
fave
good one
pra ouvir depois
max martin
infectious
fossa
aff
totalmente foda
makes me wanna kick someones ass
best bands
bruno likes this song
makes me wanna punch people for fun
best bands ever
fcking good
roquinho
nice song
makes me wanna punch someone
real indie rock
dashboard confessional
music i like
pra deixar de bom humor
should come play in brazil
musica perfeita
the get up kids
90s classic
newest obsession
synthpunk
dance - trance
seen in concert
work out music
getting over him
my fav band
love band
i loved them
triste
squallido pagliaccio
king of country
tennessee
cma awards
billy ray cyrus
metaldeath-core
folk death metal
hard rock n roolz
croozcore
progressive-mathcore
nu-skool hardrock-core
j-pop acoustic
lets go girl
oh flossy flossy
girls like
good girl gone bad ah-hah
im breaking dishes up in here all night
ae
uhull
crunck
rebeldebr
hip hop sul
tri hop
rock sulista
skiffle
perfect voice
ac/dc
nick cave and the bad seeds
seychelles
amelie poulain
sp
easy core
electrocore
pop glamour
derek sherinian
payback time
marya roxx
scott metaxas
paul crook
kevin shirley
russian rap
armin van buuren
poets of the fall
synth-gothic
gabriella cilmi
cahill remix
defender
alien girl
ben cristova
ten
on a magic carpet ride
cosmic
machine
florence
cartney
sparks
jordin
bettlefield
alo
mulher
galera
fruta
beijo
teu
lonely
cd
and
we
then
in
better
lewis
leona
dane
colby
i
rumors
dirty picture
fight or flight
robyn
maddie
alternative eletro rock
yeah
nouvelle scene francaise
sucker punch
emily browning
portuguese band
sbh
this is my favorite song in the world
yay for eagles
southern rock is coolio
mmm i love them
rocked
love em
the best british band ever
lovelovelove x 392834
pretty much nifty
i love frank :p
best everrrr
loving them
awesomely wonderful
nate is my lover :p
lara fabian
urban ac
adult alternative rock
lucie silvas
baroque and chamber pop
rod stewart
pop - rock
blitz playlist
succulent 3s
electronic-pop
psychlist
modern-classic
could be mistaken for sth else
a rebours
neo-classical underground
city names
afterglow
i hope you get shot
terrorcore
ootcore
wonkacore
i detect massive win in this song
fast and danger
muslim rave
whatcore
psychosis
tron
worth a second look
the type of woman i am
magnificently mediocre
seraphic
great covers
phosphorescent
the wicked messenger
uhhhh
sampled
slap bass
nice beat
hangover music
post-gaze
pirate punk
troglodyte music
guitar hero eat my shit
tron-gaze
look what thoughts will do
white lightning
werewolves
reversed guitar
spooky
this is good
quite strange
werewolf
true goth emo
tech - prog death
albums
toto cutugno
italiano
amici
poppyfields
merlin moon
fall into place
john lennon
merlinmoon
it brings tears
poem with music
pink floyd with syd
2011 domination
legendtina
christian pop
terrible
godneypwns
persian pop
soft-rock
nl
must see again
dansk
must see
roskilde festival 2010
sheeps
pocahontas
de
fim
rapaziada
2
francisco
filhos
justin
lazee
justin bieber jaden smith
moon
sao
up
the one
olhos
go
never
tino
coury
adriana
noite
best music ever
giane
lil mamma
anne murray
acoustic folk
nick drake
sadcore
sad music
elliott smith
iron and wine
gentle
leonard cohen
folksinger
michael cardenas
sade
giuseppe verdi
tomas mendez
musikfuerrundumdieuhr
i fucking love this song
wonderful music
so tolle musik dass dafuer mein kater sogar seine milch opfern wuerde
dredg
insanelymiraculous
a perfect circle
archive
beautiful track
radio disney
marry me zooey deschanel
stab me in the heart
favorite albums ever
ewwwwwwwwwwwwwwwwwww
post-indie
eletro-indie
uhuuuuuuuuuuuuuuuuuulll
post-prog
listened more than 10 times in a row
old time rock n roll
covers that are actually good
taking over my mind
metal meltdown
ill be ok
teen days
bright and lovely
good morning
female vocalists i actually like
by evtim evtimov
love songs suck and fairytales are not true
britney spears ate too many raw eggs and her voice mutated
insane classical motives
anti-nostalgia
please plaese please only play instrumental
my day sucks
now i can dance
stuff i should be able to inject directly into my blood
by lubomir levchev
retarded
hell fucking yeah
silent hill
standards
pre-rock era
intrumental
exotica
library music
northern soul
techno brut
80er
hang on
rumpelmusike
ur
80s alternative
deutscher pop-rock
kittyfish
i love the kinks
techno classics
george harrison
download
mp3
body
mo
2step
halo
tracks
son
track
step
steam
soul female vocalists british singer-songwriter
zmierzch
lostprophets
meu tudo
minha tesuda
minha gostosa
bg
star academy
american music
male artist
girlsband
movies
gilmore girls
boysbands
hermoso
hope
rico
just love
de eme amore mio
pena
para fornicar
suicidio
be-my-baby drum
two bands with same name
eclectic
mountain road trip music
suicide songs
sci-fi beats
modern mantras
zoe
rojo
se cura
tvn
herida
una
jaci
como
canto
teleserie
silberstein
alana
camila
skin yard
rapp metal
punk rock fusion
funkpunk
music to make you think about shit punk
hardcore noise
gutter metal
manualist
brent shrewsbury
better than shitsmack
river city hardcore
black oak arkansas
southern punk rock
nashvile pussy
son of slam
free jazz and confusion
jesus lizard
hard line
maverick records
bulb records
mind funk
mighty unicorn
california speed metal punk band
the golden age of pop
golden oldies
mall metal
female beck
alternative funk jazz industrial noise fusion
posermetal
teen punk
new moon
rhythmalizm
rhymez
whitenoize
old skool
funkalistic
bone bone bone
liiive
not rnb
big bands
gta4
hip hop classic
1-2-cha-cha-cha
funkalistik
quickjazz
big band swing
g
swing jazz
timba
cartoonz
keys
emma bunton
moldavian
afghanistan
afghan
none
fave tracks
fave tracks two
britney rulzzz
britney rulzz
the best group in the world
romantic rock
indie-romantic
rock-pop
love-rock
sertse
mustafaria records
mustafaria
serdtsebienie
heartbeat
music from soul
records
latvian rock
total
jay chou
zveri
afro-fusion
lugansk
turk rock
anadolu rock
ozgun akay
crybs choice
turkish punk
sensin vajina
rok
kimdi
elektronic
tatu
cat stevens
dido
awesome bands
alternative heavy rock
alt-rock
faves   favorite
male vocalists   melancholy
alternative pop-rock
proto doom
baseball
tony iommi
melodic deathmetal
doommetal
pue chaos
harcore punk
soviet kitsch
hiv
hiv infected
ihaa
russian underground hip-hop
hate-hop
1488
rap russian
anarcho-chansone
vegan straight edge
moscow rap
electro hop
cory
chord overstreet
fresno e cabal
heather morris
divas pop
blair and chuck
soundtrack musical
punk goes
dreamgirls
coca cola
ftsk selena gomez
beeshop esteban
abril
fresno
cast disney
disco rock
elektronik
damar
nacionais
independente
the libertines
self-made
salvador
people i have seen live
brazilian emo
rocknroll
musica para beber e brigar
sussas
violino
lindo
incomparavel
perfeitaaaaaa
brian littrell
howie dorough
aj mclean
this is us
black holes and revelations
tv shows
classic hollywood
radio:active
variete francaise
thick as fuck
bliss
freaky as hell on visualizer
blast it
beachy
postrock
start a riot
sounds like iggy pop
sounds like matthew dear
noisepop
psych folk
steel drummy goodness
destructed
walking in a moonlit jungle while toxic spores tumble from the canopy
melhores
british indie
extraordinary indie
country: canada
country: usa
country: jamaica
hawaii
country: britain
dub poetry
country: guyana
soca
country: the philippines
country: italy
country: dominica
country: burkina faso
country: slovenia
country: argentina
country: germany
country: new zealand
country: australia
country: france
country: ghana
country: brasil
country: honduras
mr mathers
dr. dre
legal drug
chamillionaire
wish list
recos
alsolike
bestofjazz2009
progarchives100
top100
symphony of decay
best drummer ever
best band youve never heard
bemis
grade
angular
dirge
fucking hilarious
absolutely amazing
explosive denouement
original screamo
uk artists
male groups
fashion girls
girl thing
atomic kitten
j-core
k-hip hop
taiwanese
c-hip hop
jhiphop
ohmydamn
cah
mojo presents subpop 300
gustavo cerati
soda stereo
rock argento
blues argento
rock psicodelico
rock en castellano
rock fusion
electronica experimental
electronic progressive jazz rock
psicoelectronica
folclore argentino
blues castellano
sonico
candombe
fusion african
melodico argentino
bossanova
jazz pop
harsh noise
experimental hip-hop
noisegrind
death industrial
splittercore
jukebox project
i buy singles for b-sides like this
tambourine
vaguely homosexual
songs i sing in the shower
finger snapping
cute but slightly sinister
like crack for my soul
one-two-three-four
unintentionally hysterical
multiple jarvgasms
levi stubbs is the fucking man
choreographed dance moves
songs that have whoo in them
gleefully sexual
get a job
xxxmas
dreamy-perfect pop
telephone numbers
sexy falsetto music time
the subjunctive
puppet on a string
prague spring
pop gladiators
jam hot
wells running dry
rudyard kipling
name-drops camus
phallic trumpet
joygasm
70s-80s uk punk and post-punk
doctor syntax the warforged bard
hot for teacher
sexy bitch
babe of epic proportions
love this song
art metal
depression is kicking in
favorite tool songs
grew up on
heroin
drum solo
indietronic
j-pop japanese pop jpop female vocalists jazz japanese music j-rock
pop female vocalists rock dance alternative hip-hop female rnb
female vocalist ballad 90s guilty pleasure american sad 1999
pop rock female vocalists canadian punk alternative pop rock female
rock pop alternative alternative rock indie pop rock punk seen live
j-pop japanese jpop pop female vocalists japan japanese music asian
j-pop japanese jpop pop anime j-rock female vocalists japanese music
j-pop japanese jpop pop anime japan hip-hop female vocalists
k-music   k-pop   korean   kpop   my gurl   ost   soundtrack
french electro
figure it out
black classical pianist
jazz vocal
yumiko
iq final perfect music file
avant pop
real yumiko
pop trash
shiina ringo
beyond their time
perfect duet
just brilliant
real tokyo jihen
punk sound
bishoujo senshi sailor moon
surfcore
neo punk
definitely not john zorn style
waverace64
neo hardcore
demoscene
too math
south coast
gasshou kumikyoku
silent era
japanese punk
japanese rockabilly
sorry
dream-sleep
early music
unkle
glass
my arctic monkey
astrud gilberto
top40
britneyspears
lil mama
will smith
male vocalist duo
triple j
beautiful music
depeche mode favourites
incredible voice
prata vetra
lovely englishness
euro trash
rude
buggery
out of ether
nashville
out of ether and friends
itunes
broadjam
rob thomas
darius rucker
nashvegas
matchbox 20
matchbox twenty
outofether
jason mraz
facebook
appleton
ryan adams
myspace
train
green bay
john mayer
cancer research
cancer awareness
one republic
jimmy buffet
band perry
bon jovi
goo goo dolls
hootie and the blowfish
stolen song
the band perry
academy of country music awards
martina mcbride
sarah evans
trisha yearwood
oxynucid
uk bass
mrs jynx
improvised
vertical67
schlacht
fav song
beautiful sad songs
nice vocals
memoirs of imperfect angel
hurts when i listen to it
i could listen to this forever
could listen to this forever
so deep i almost drown
lenny kravitz
reminds me of summer 2008
looooooooooooooooooooooooove this song
i adore u
songs that are so pretty they almost hurt
another reason to live and love
laying on the grass and looking at the sky
let me sign
freaking addictive
too nice to listen
excellent and beyond
hotness
music makes you lose control
social degradation
fav chalga song
o despertar da primavera
part of my whole life
spring awakening
summer breeze
bright side of life
shake your money maker
listenable emopop
billy howerdel
modern alternative
nothin on but the radio
erikka supernova
kraby the klown
666
buttercore
crunkcore
gothic glam
scene band
prostitude music
aiden
afi
cannibal corpse
cm punk
undead
hollywood undead
up beat
guitar band
top tune
ross and the wrongens
radio friendly
great sound
love life
dirty southern
crap live
saw at glastonbury
clickpop
bellingham
sexy fucking dance
girl electro rock
idaho
zombie rock
looper
eastern-influenced
not-the-ohio-band
broken-up
compilation
diverse
indie guitar rock
hip hop techno electronica beggars rap
vampiric metal
psychdelic dark ambient sludge avant-garde experimental doom drone metal
70ss
zalim metal
doom rock
80ss
balyoz metal
hele hele metal
ustad
jeff martin
ethnic punk
hobarey
heavy doom metal
acisiz arabesk
psychedelic dark ambient sludge avant-garde experimental doom drone metal
metalocalypse
lise yillari
led zeppelin cover
bang
cock fight
early 80s
progressive ambient rock
come on you irons
pot goes to 11
triple sec
do smp
martwe
niebka
bitmaszyna
twitch glitch
yass
united kingdom
austria
psychosolstice
pinkroom
pink room
psychodelic
path of dying truth
king crimson
frippertronics
korn
old school hip hop
kittie
scarface
nightmare before christmas
covers that are far far better than the original
gta soundtrack
gta iii
xzibit
mudvayne
run dmc
limp bizkit
nfsu 2
escape the fate
00s rock
love making music
irony
clever
synth-funk
chillosophy
sountrack
nine
actresses
garbagebagwindow
garbage bag window
peng
summery
wishful
subtle
delicate
love itt
tune
michael jackson bad
speechless
euphoric
heaven
merry christmas
christmas song
christmas album
denters
esmee
outta
here
mother
mum
vma
primer amor
loves embrace
beautful
get yours
get mine
cruz
loving me 4 me
can't hold us down
i'm ok
make over
impossible
the voice within
soar
walk away
underappreciated
the most beautiful fag in this world
mainz
sexyy
daring
glamorous
outrageous
90s lovee
alternativee
emo x3
wowtastic
x3
hung up
princess of pop
jammin
fab
edgy
birth year
feel good inc
classy
golden state
paul oakenfold
emma watson
elijah wood
ashley greene
natalie imbruglia
twilight soundtrack
this is not emo
synth gothic
melodic punk
imperative reaction
the bravery
hypernova
elsiane
we have band
provision
hurts
santigold
lowe
whistling
heart-breaking
unstoppable
tear-jearker
johnny cash
toto
galactic
lights
naked
question
vampire
cansas
dorothy
stand-out
too late
purple haze
eternal love
video games music
poota
cabelo ruim
rampeira
ridicula
viadinha
demonia
sandy
ex-sandy
gostosa
ahazante
amo demaix
gozada
vadia que faz musica boa
lenda do folclore brasileiro
curupira
testa
voz da decada
gorda e gostosa
golfinho
obesa
baleia
chubby
moving my hips like yeah
jeff buckley
historic
cossack
vaticanian
videogame
the sims 3
children's
duda kesnay
auto didata
autodidata
thrash/death metal
satanic black metal from hell
a god on the earth
a floyd
death brutal pagode
psychodelic internet
feijoada bulgara
troll
stone rock
brit-rock
heavy blues rock
the rocket summer
chick vocal
dream pop  electropop
the chocolate
outcesticide viii
the pixies
california gurls
high school musical 3
all artists
beardy
rock   seen live
indie indie pop acoustic mellow norwegian alternative indie rock folk scandinavian
manchester sound
sad and beautifull
sheffiel bands
indie indi
super fun
coyote ugly
finest tracks
greatest sad songs
amazing songwriters
proto metal
traditional heavy metal
heavy blues
old school heavy metal
horrorcore rap
bone thugs
awesomeness
mona lisa
emo alternativo hardcore pop rock
thai-pop
loveis
joey boy
luktung
hardcore metal
thai rock
yearns to be covered
sex music
nighttime
stuff that gets me moving
holy fucking shit this shit is fucking awesome
bloody great
build up
fav
distorted
disconnection from reality
space tech
i would totally love this a million times
essential instrumental versions
the end
exclusive
sleeptronica
deep sea
outro
intergalactic hip-hop
interesting and will do more research on this artist soon
get on this
ender thomas
bratz
five
venezuelan
yanni
leslie mills
yanni voices
chloe lowery
nathan pacheco
mia sara
nx zero
good old fashioned life afterbirth
good old fashioned death
good old fashioned life
good old fashioned birth
johnny atterbury
wesley jay
kent dylan
reigndear
wee embers
fine champagne cognac
sleighbells
taiwan
east london
recomended
the second of the fourteenth
royalties
blackeyes
luap nhoj
oh cigarette
your imagination goes here
for memory
hurse
fourteenth
official album
snowowl records
beacon bay
the london school of music
limited edition
rip out my fucking heart why dont you
fucking mental
fucking annoying background tone
riceboy sleeps
a crock of crap
their lyrics seem to describe how i feel
post acid
nome escroto
numeros
tosqueira
the best metal band
gloria
satanic metal
the best of bad religion
the best of green day
rehab
girl-band
latavia
destiny's child
girlbands
br
quissu munique
qqqqqqqqqqq
talento
perfect ballad
podre
rotten
stuck on repeat
cant stop listening
jornalismo
hard grunge
samba-rock
beautiful voice and guitar
new mpb
american jazz
brazilian jazz
modern ska
mpb christian
acoustic christian
brazilian ska
pre-grunge alternative rock
relax music
cavalera
alternative & punk
akiko shikata
aoyama thelma
thelma aoyama
myth
unforgattable
brit style
best dance track 2009
exposure music awards
melody pop
local bands
workout songs
peter paul and mary
we gotta fight fight fight fight fight for this love
come here rude boy-boy
revoltads
passiva
poppunk
oldschool punkrock
77 style punk
awesome punk
old-school punk
magyar
us punk
melodic punk-rock
lovable foreigners
old skool punk
lookout records
pop-punk fury
punk rock show
punk-pop
magyar punk
hungarian punk-rock
punk revival
good punk
true punk
hungarian punk
buzzpop
punk rock-n-roll
lookout
die Ärzte
clash
singalong punk
mississippi
old blues
the mars volta
working class
old but good
super catchy
me likey
so sweet
jackass number 2
fucking beast
kinda emoish
future wifey
hate her but like her music
hated him loved his music
sounds better live
people are crazy
reminds me of my besties
rip
sexi
sexy video
cute song
i cry when angels deserve to die
lovemetal
gulity pleasure
jared leto
sexy lead singer
love her voice
eazy-e
ice cube
dr dre
temporal
techno-industrial
dark industrial
industrial noise
hard dance
hip-hop industrial
electro - industrial
ambient-industrial
industrial world
el nin yo
agrestina
paranaense
music for your ears
melodramatic popular song
roquenrou
parana
pr
makes me want to shout it from a rooftop
me faz querer subir no telhado e gritar pra todo mundo ouvir
one of the best tracks ever
isolation
ian curtis
she wolf
man vocalist
good rnb
relationships
rock out
angry
mystical
innocent
philosophical
so cheesy
ooh girl
blunt
wise
reflective
release
confessional
little mermaid
british television
harsh
electrodark
electronic house
white rap
trashbag filled with vomit
sondtrack
tocante
kid abelha
patti smith
perfeita
chav
melodic thrash metal
brian hands
chamone
godlike people of awesomeness
1800s
global
dig it
well cool
farout
lms artist
west yorkshire
bradford
elegiac
alternative soul
guitarishness
party punk
twin guitar
mixed vocals
girls aloud
coo
european song
speak now
fearless
kiss and tell
dignity
meet miley cyrus
all i ever wanted
extranjera
a year without a rain
breakout
depre
corta venas
dedicadas
myfavorite
sad nights
para llorar
para bailar
ghost rave
grave wave
dark shoegaze
dubtech
dub tech
uk funky
balearic pop
psych-pop
belgian screamo
malaysia idol
jihad
electroturca
naat
military march
jihad song
le saint quran
ottoman military band
free indir indir
ottoman traditional
ucretsiz mp3
pakistan
jihad songs
ottoman classical
mehter
naathan
ottoman art
mevlidhan
ghazal
moroccain
enshaad
spiritual trance
united arab emirates
kuran
saudades
cocaine
brasileiras tem boom boom
really great punk rock
the best punk rock album of 2006
in-fucking-credible
h
deutscher rock
kultrock
bad reptile
epic ballads
mossy
mossy rock
bff
my bff
neverever
bff band
bff rock
444
masaru
528hz
funk folk
chat
secret
room
chatroom
528
444hz
emoto
childrens
sesame street
zombie
bacon
fever
frequency
vibration
its me bitches
its me bitchs
oldie
cats
white metal
lefties
anarchist
d-beat
songwriters
muka muka
jolly
oyaebu
songs that make you wanna get up and dance
tetris-core
8 bit peoples
naturally
hardcoregutural
fd
brazilian rulez
rap eletronic
kelis - i want your love
dawn of ashes -angels
deftones  root
skatepunk
dream street
native american music
jermaine jackson
guyana
guyanese
funky pop
jason white
fob
guitarrist

think geek
my shoes are speaking
strange voice rap
brainy
mf doom
toy
new-hop
locking
fix your damn tags
insane guitarists
posteverything
dub techno
popping
drumz
free album
to write love on my arms
marissa cooper
quod me nutrit me destruit
soundtrack of my childhood
my own private sunshine
effy
happygoodfunandhandclaps
must see live before dying
always be my konstantine
minskcore
jennifers body
classical music
leftovergrooves
musthavealbums
groovesuspects
myupbeats
smooth reggae
collectables
artists of my 80s
maghreb
tunisia
berber
tuareg
kabyle
desert
sahara
marrakech
algeria
rabat
tanger
world groove
oran
alger
sousse
rai
amazigh
maghrebi
mediterrania
orient
casablanca
arabica
imazighen
rif
drum bass
travel
blissrock
newgaze
kill rock stars
newgazer
rural colours
italian prog
oriental metal
mutant hop
undergroung hip-hop
hulehap
hip-hop kovsh
graffity hip hop
khimki rap
under hop
zalepi svoe dulo
new school
shanson
black rap
miami bass
bee gees
flower power
dj's
v-ice
south a1a
babywhoo
radio free download
free song download
funky house mxes
bsp
ic
i see you
icu
iseeyou
iseeu
hip hop soul
mandopop
alternative hip hop
dance-rock
am
grindgore
gooosecore
out
street
eat me
should not wear clothes
delicious
creativity
brazil queen
should win american idol
queen of flop
fuck the other tags
kings of flop
next queen of pop
you cannot listen to this without singing along
mothers day
first track scrobbled in 2010
first track scrobbled in 2011
dont judge an album by its cover
lady delish
funky white bitch
rurouni kenshin
asia queen
top cd
french girls
ayumi hamasaki
avex trax
yui
btvs
wicked women
winnipeg
live music archive
dimeadozen dot org
matts
matt braunger
matt dwyer
matts radio
ast
a special thing
daily show
wyatt cenac
atmospheric drum and bass
classic house
jazzy house
detroit techno
broken beat
ron trent
drumfunk
progressive breaks
disco house
intelligent drum and bass
london symphony
american vocalists
brazilpop
mel c
symphony orchestra
london voices
star wars
ursa
whisper
curitiba
velha guarda
doce
campeao dos campeoes
a mais temida
tricolor paulista
sao paulo futebol club
bonde do mal
sweet music
amazing girl
georgian blues
teenagerpunk
ironico
banda do sul
vamo ser campeao
indie jazz rock
indie folk acustic art
electric fuck
sarcastico
brasil-franca
demode
classicorockinroll
indie rock blase
eletro-samba
new folk
garage-rock
nao me amole
german rap
ey yo
punchlinerap
german dream
düsseldorf
st. pauli
german hip hop
godsilla
offenbach
frankfurt am main
freiburgs finest
queensbridge
queens
perfect album
lisa taylor
beach music
don dixon
post-replacements
skowee
egzamin
polecane
hype
sugary
dancante
songs that cheer me up
whoah yeah
songs that are important to me
elecronica
happy thoughts
look at the sky tonight
simply amazing
fuck it all and lets dance
my love
jared is hot
makes want to dance
i would give this two hearts if i could
quit these pretentious things and just punch the clock
we run away but dont know why
80s songs not actually made in the 80s
songs to sing in the shower
songs that makes you want to be rich
fell in love
songs that describes your life
better than your music
early mtv
sun
listen to again
nothing you said today is gonna bring me down
amazin beat
songs are like time machines
emo by april
dead by april
melodic alternative metal
pop death metal
pop metalcore
ou nao
rs
barroque
irish folk punk rock
this is a song about being happy
romantic love songs
tru madafaking dark anal bleeding porngrind
hier koennte ihre werbung stehen
jewbashing
kirov
actors
mine
visionary
seen live but wasnt quite sober
riqueza
seducao
better than bjork
good-weird
alias
style icon
female of sorrow
la diva turca
surunuyorum
sing for peace
makes you dance
ajda forever
voice of beirut
boyfriend material
lollipop
film music composer
hk-pop
j-electro
angura kei
ps company
g-zas
jap rap
visual key
ruki
the gazette
blaarty hardcore
experimemtal
dub-step
internally drilling techno
riot
ayu
berryz koubou
c-ute
ami suzuki
momusu
buono

hello project kids
ai otsuka
nami tamaki
snsd
freakshow
music for car
lemme tell you bout a girl i know
power groove
alternativ
evil disco
los hermanos
one love
not-emo
songs to download later
songs that make me happy in the pants :d
songs that make me want to make love
its not fucking emo for petes sakes
songs that make me laugh til i shit myself but are still good songs
overplayed
yay
i like girls
impressive
toe-tapping
songs that would explain my life with the lyrics
wicked not well known bands :d
songs that make me want to eat cake
1337
leet
1337 guitar players
awesome in your face
luff this song
its three seconds in and i already like this song mucho mucho :p
man beating a goat
orgasm-inducing-ska
gay church folk music
the very best song ever with no argument whatsoever :d
songs to try and figure what amp theyre using cause its cool sounding
the ultimate amazing bestestest song evar
lesbians
songs that make me feel like a man
moe
the melancholy of suzumiya haruhi
asahina mikuru
mai otome
ryuichi sakamoto works
kiyonori matsuo
haruomi hosono works
happy end
a long vacation
docomo cm
808state
female drummer
j-house
cinema
ryomei shirai
beatniks
masamichi sugi
niagara
martin lee gore
evergreen pop
takkyu ishino
tsutsumi kyohei
yasuyuki okamura
yukihiro takahashi
hosono haruomi
trex
alison moyet
emotional song
trucker metal
gore
heavenly voice
renaissance new age
dead music
super sexy sweet bornt honey skin j-pop star on the extra-high heels
trip rock
industrial blues
effortlessly cool
about dreams
reik
favela chic
vozes do morro
commercial
mainstream
discovery
discovery channel
david ayers
the shift
felix tod
awesome-ness
the songs that make me sick with butterflies
hand claps
venus rock
baby making music
forro pe de serra
coco
why is this only a bonus track
future hip-hop
chi-town
hips
beautiful melody
amazing collaboration
sunny day melancholy
life saving
soothing nightmare
entrancing
dark magic
lovely sadness
oh how precious
forlorn road songs
beautifle
seth cohen would listen to this
music from the oc
bands that start with the
boy with guitar
angry chick music
getcrunk
new york rap
andrew mcmahon
country southernrock ugamusic
rnb breakupmusic
souther rock
corey smith
uga music
rockstar:supernova
lukas rossi
rufus wainwright
dance dance dance
courtney love
oh canada
something that makes sad things ok
living in the suburbs in the 1980s
hollywood
moms vinyl collection
absoluta -q
aaaaa
romantico
day26
stadium music
forever in a day
making the band 4
girlamazing
heartbroke
smash it up
dj gold
synth-punk
potty mouth
new cross
fuzz
mstrkrft
the smell
toy soldier records
musicteam motivational
helentest1
never have good speakers mattered less
all-girl
remix of health
sugababes
inspirational songs
sexy and fierce
kind of beautiful swoonworthy and timestopping
synth attack
sexy guitar hey
incredibly sexual
jet set radio future
electrolash
ozzy osbourne ozzy metal heavy metal zakk wylde zakk
zakk
ozzy
mike bordin
morumbi
eu fui
raridades
smoking music
guns
axl
9th strikes again
modern day boom bap
unknown gem
premo
just blaze
nigga got socked
best rapper ever
trap musik
thank you based god
grimey
lyrical homicide of 50 cent
epitome of hip hop
sid roams
dj khalil
makes me reminisce
nasty nas
life goes on
diamond head
machine head
black shepherd thrash
deliverance  thrash
metal church
nevermore
marty friedman
cacophony
evildead
kvlt hipster metulz
avenger of blood
attacker
beautfiul intro
nothing is better than this
i love this
in love
soo much win
80s pop metal
electro-folk
electronic hardcore
brown
glitchcore
metro downtempo
late night
disco bullshit
tekk
tekkno
weekly top tracks
weekly top artists
essential madonna
brings out the kid in me
powerfull voices
hecho en mexico
cocksucker
tianguiscore
tinosoft
puta de mierda ojala y te mueras con un pinche dildo electrico atorado en tu ronosa pucha
pasito duranguense
best free stuff
albums i bought
mexican folk
este disco me hace sentir un orgasmo
mexicano
duranguense
xd
una mamada
huapango
todaysart
the hague
just good music
underground resistance
club transmediale
bunker
den haag
electrofunk
mutek
the west coast sound of holland
loops
this world go crazy
the people united  will never be defeated
brenna maccrimmon
dave mustaine
80s rock
speed
quick
james hetfield
90s metal
lars ulrich
kirk hammett
famous
thrasher
marty
glamour
retropop
nepali rock band
nepali
song for peace
nepali instrumental serene ambient
nepali guitar instumental serene fusion
ben lee
butterfly boucher
thirteen senses
longview
songs that sexually excite me
favourite sad songs
depressed mode
so gay
beautifull
voice lust
wkurw
power dance pop
international
amazing pop
fascinating
ethnic chill
angelic
etheric
mythos
cordovero
melodious
heavenly
old school hip-hop-inspired
best of hip hop
street music
most underrated artists
flow like the blood on a murder scene
hip hop and rap
dmx
gansta rap
real rap
my favourite rappers
no hoodie
wu fam
madlib is like awesome
deutsch rap
deutsche hip-hop
eskibeat
need my guns back
eski
hip-hop alternative talib kweli stones throw records madlib east coast
j dilla changed my life
depeche
meat
twitch
strip
bone
rawr
pretty much amazing
my most favourite song in the entire world
awesome drums
i can play the drums
ragga jungle
underoath
songs that make me feel better
ponyrape
intense-mutha-fuckin-power-violence
:3
my ballz in your mouth :3
shit music is shit
nigga stole ma bike
hard bass
analrape
north korea is best korea
shitting dick nipples
lulz
i want her ass
a honey glazed cock ramming into your ears multiple times
norteno
fgsfds
shit only a retard would listen to
4chan
corridos
nortenas
i accidentally on justin bieber once
gay metal
accordion
dane cook
suicidal black metal with a touch of porngrind and synthesizers
in my head forever
recife
unicef41
fankadeli
andreea banica
inna
mulatos-muzsika
right round
notar mary
modern elektronikus tanczene
pantera - domination
drowning pool
no genre can describe
sleep music
gives me chills
music to kill yourself to
toured with
dark as fuck
electroacoustic
live electronics
rune grammofon
utorn
netaudio
tunes
indie electronica
weblabel
ua
buriedcable
yarema
ragged
intelligent dance music
indie-electronic
thorshji
kiev
scrap basket
sviter
free donwload
smart
smart electronics
kyiv
dumb
audio
neuro
pianist ep
nnnngh
rrarrrr
samba rock
singersongwriter female pop
instrumental piano
fresh love
new wave and eighties
other great tracks
metal for fish
deadened black metal
jamtronica
smash your child against a wall
blacker than the blackest black times infinity
all the people you love in a river of blood
devy metal
derp
vfsix
launge
light trip-hop
moscow
pat appleton
goldfrapp
emilie simon
morcheeba
mandalay
hooverphonic
zero 7
urban myth club
smoke city
peace orchestra
6-string bass
lunascape
pati yang
helicopter girl
lost balance
dark psychedelic trance
full on morning
perfect songs
teenage
spice
cool songs
my life is here
i own
beautiful songs
vicio
best dance singer
heidi
bitchney is back
estranha
putaria
xxxxxtttiiinnnnaaaa
bonustrack
boom boom supersonic boom
putinhas
sonny with a chance
fuckin perfect
4ever
breakaway
siobhan
keisha
amelle
mutya
jade
favorite bands and artists
brooke fraser
keep ya calm
girl name songs
porque te vas
songs about girls
life sucks
sleepy time
death song
tedio
hymns
soul searching
happy melody
songs for christmas
work
nova mpb
new zeland
toxicity
hey
nosowska
kasia
kasia noswoska
melrose place
pretty little liars
life unexpected
hellcats
valentines day
american idol 8
ot7
ot1
american idol 5
the x factor 5
neighbours
nikita
american idol 7
popstars: the rivals
ot5
the last song
the x factor 6
ultimate music hit
matinee group
american idol 3
american idol 6
ot4
american juniors
ot6
the x factor 7
deutschland sucht den superstar 5
the x factor 2
s club 7
american idol 2
los hombres de paco
pantene
fisica o quimica

the x factor 4
el corte ingles
ot2
medical
tv show
serie
vasco
carmen
organ
editors
fuckin great solo
mezclas
freestye
need to listen 100 times in a row
highlife

very good
excelent
europe band
epic pop
christian-rock
back to the 90s
culture of chaos
year of the rabbit
juiz de fora
mc lord magrao
gta san andreas
radio x
grand theft auto san andreas
san andreasm
autolux
electric dreams
elektorpop
this kraft works
ney
muslim
enstrumantal
melancolia
boys with guitars
positivity
female rapper
dark house
minimalist rock
disco filter
electronic folk
ambient sound
spinetta
charly
cat power
calamaro
liliana herrero
zitarrosa
fito
vox dei
pedro aznar
sumo
leon
scofield
refexion
alfredo casero
casero
pappo
abc1
aquelarre
hilda
tension
bersuit
nina simone
violeta
spineta
joni mitchell
lovelovelove
rain is a good thang
lea
the all-american rejects
i will follow you into the dark
jimi hendrix
aerosmith
meta
after forever
tarja turunen
saving abel
anthony green
m0sh
cheetos
keyboard rock
akron
posthumous
whisper signal
down-tempo
alternative folk
shearwater
earnest
misra
sleepy
blues pop
from:usa
coachella
to discover
slow core
look into
have seen live
avant-folk
easy going
cool shit
neue deutsche harte
ultra heavy beat
victorian industrial
somafm
big chill
808 state
psy ambient
ambient-breakbeat
mixmaster morris
cerebral
lp5
incunabula
global communication
bonobo
richard d james
warp records
dj kicks
husband and wife duets
french vocalist
franz schubert
buffetlibre
blogs
amnesty international
teatro petrella
the world is round
mark plati
amnesty
alernative
amycanbe
digital
bub
progressive synth pop
bubby
bubbylicious
bubbysauras
male fronted metal
sevillanas
blondelicious
saso
fan
q
thundercats tribute
great beatles cover
lilian garcia
two thirty-six
mnogo dobro
one
syndrom ciarek
hall and oates
jewel
kane
hbk
country taggradio
figure 8
q5
most awesome person to ever exist
i know him
the oddest thing ive ever heard
chris kirkpatrick is hot
300
frickin awesome
great composers
awesome soundtracks
molested ken casey
celtic punk
summer feeling
mega mix
synthie pop
smexiiness 8d
englisch
mittelalter
best songs
electrogoth
neue deutsche haerte
ndh
crossover thrash metal
nigger music
macumba music
abba cover
worst song ever
prom
songs i never get sick of
isa
pretty fucking awesome
bitter sweet
sanna
lmfao
recommended to me
johny
i melted
vacation
in bloom
every song is great
mom
south pride
buenisisimisima
mexican pop
kot
tarkan
latin diva
international pop
avenue q
puppets
explicit lyrics
elis regina
gothic industrial
original hard rock
my future husband
blues metal
italian music
festival italian music
japan musica
jazz trompete argentina
the spill canvas
the academy is
gym class heroes
something corporate
mindless self indulgence
the cab
jacks mannequin
mandy moore
teddy geiger
my american heart
cute is what we aim for
bucky covington
jamisonparker
porcelain and the tramps
kellie pickler
ima robot
rediscover
shiny toy guns
jason castro
the faint
deadstar assembly
head automatica
be your own pet
jack's mannequin
ryan gosling
rooney
nightmare of you
thursday
sugarcult
arctic monkeys acoustic
emm
estranho mundo
independant
all album for free
site officiel : yeffi net
funky fresh
sludgecore
otto dix
funreal doom
punk-hop
ebet
avantgrade
batcave deathrock
second wave goth
brucecore
punk cabaret
fitness rock
primus
angry
hanoi rocks
christian & gospel
ezbeni
winds of soul
devrim
derd
gluhie 90e
scott pilgrim
suomi
symbiosis
sverige
eire
electro latino














# if such word is found, add the tag
tag_values = df_tags['tagValue']

tag_values.to_csv('tags.csv',index=False)






# tag preprocessing
df_tags['tagValue'] = df_tags['tagValue'].apply(lambda x:re.sub(" albums| songs",'',x))
df_tags['tagValue'] = df_tags['tagValue'].apply(lambda x:re.sub("(\d+)( ?'?s)",r'\1',x))
df_tags['tagValue'] = df_tags['tagValue'].apply(lambda x:re.sub("#(\d+)",r'\1',x))
df_tags['tagValue'] = df_tags['tagValue'].apply(lambda x:re.sub("'(\d+)",r'\1',x))     
df_tags['tagValue'] = df_tags['tagValue'].apply(lambda x:re.sub("^(\d{2})$",r'19\1',x))  
df_tags['tagValue'] = df_tags['tagValue'].apply(lambda x:x.replace( '1200 mics', '1200 micrograms')) 
df_tags['tagValue'] = df_tags['tagValue'].apply(lambda x:x.replace( ' in at nutshell', ''))  
def unify_epoch(epoch_value):
    try: 
        int(epoch_value)
    except:
        return epoch_value
    epoch_value = int(epoch_value)
    epoch = ''
    if epoch_value < 1900 or epoch_value >= 2000:
        epoch =  epoch_value
    for i in range(1900,2010,10):
        if i <= epoch_value <= i+9:
            epoch = str(i)   
            break
    return str(epoch)
df_tags['tagValue'] = df_tags['tagValue'].apply(lambda x:unify_epoch(x)) 
df_tags['tagValue'] = df_tags['tagValue'].apply(lambda x:re.sub("^(\d{1})-?s?tars?",r'\1 stars',x))    
df_tags['tagValue'] = df_tags['tagValue'].apply(lambda x:re.sub("^(.+) ((songs?)|(albums?))",r'\1',x)) 
df_tags['tagValue'] = df_tags['tagValue'].apply(lambda x:re.sub("^-(.+)",r'\1',x)) 
df_tags['tagValue'] = df_tags['tagValue'].apply(lambda x:x.replace('-',' ')) 
df_tags['tagValue'] = df_tags['tagValue'].apply(lambda x:x.replace('  ',' ')) 
df_tags['tagValue'] = df_tags['tagValue'].apply(lambda x:x.replace('psychedelia','psychedelic')) 

invalid_string_list = [
    '0 play yet',
    '1 2 cha cha cha',
    '1 giant leap',
]
for invalid_string in invalid_string_list:
    df_tags['tagValue'] = df_tags['tagValue'].apply(lambda x:x.replace(invalid_string,None)) 



list(df_tags['tagValue'].sort_values().unique()[:400])


'''
código não considera cameos entre as bandas/artistas
nao leva em conta temas e sub temas nas tags
codigo nao leva em conta falta de padronização entre tags
nao leva em consideração nacionalidade

nacionalidade e integrantes como tags
tags expressando sentimentos ¬¬
tags trolls

tags-> realTags: subtags, tags padronizadas
artist -> artistTags: integrantes, temática, país de origem, ano de surgimento


'''