# Last FM Artists Recommender

Primeira versão de um sistema de recomendação envolvendo músicas. 
Utiliza Web Scraping para coletar informações de artistas do site LastFM.
O sistema tenta encontrar artistas/bandas similares com base na banda que recebeu de entrada.

Uma versão mais complexa do projeto pode ser analisada em https://bitbucket.org/icaro_marley/music-track-recommender/

# Tecnologias utilizadas
- Scrapy (Web Scraping)